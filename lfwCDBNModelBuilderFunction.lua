
--require 'nn'
--require 'image'
require("posteriorProbabilityActivationsFile")

function CDBNModelBuilder(L,poolingRatioArray,filterCountArray,convFilterSizeArray)
    --Start with some arbitrary parameters - will function-ise later.
    -- 2 hidden layers - K_1 = 32, K_2 = 96, i.e. L=2
    -- C_1 = 3 = C_2 for pooling
    -- N_w1 = N_w2 = 20 to start

    --setup parameters - these will be input to the function 
    --L = 2 --i.e. L is the number of hidden layers.
    --filterCountArray = {32,96}
    --poolingRatioArray = {3,3} --Assuming square pools
    --convFilterSizeArray = {20,20} --Assuming square convolutional filters

    --NB, if the filter sizes and the images don't quite line-up then we might lose the information at the edges; 
    --  could use padding in this case but let's not worry about it now.
    --  i.e. for a 7x7 image and 3x3 pools, we'd lose the rightmost and bottom-most edge of pixels.

    model = nn.Sequential() --oops, set this to a function declaration first time around
    model:add(nn.SpatialConvolution(1,filterCountArray[1],convFilterSizeArray[1],convFilterSizeArray[1]))
    --Once the spatialConvolution layer has been applied, we now need to figure out the probability of posterior
    --  unit activation, which is what this next chunk is for.
    --  First, we apply exponentials to each term, then sum over the pools. This is done *slightly* hack-ily, since
    --  we use the spatial average then multiply back up to get the sum. Depending on how the spatialAveragePooling
    --  is carried out this may introduce some bias at the edges of the image (e.g. if the spatialAveragePooling
    --  operation is smart enough to divide by a different number for the edges then we have said bias.)
    --  Carrying on, we then use SoftSign() (for each term in the tensor X we calculate x_i/(1+|x_i|)), which is
    --  our posterior unit activation probability.
    --  
    model:add(nn.Exp())
    model:add(nn.SpatialAveragePooling(poolingRatioArray[1],poolingRatioArray[1],poolingRatioArray[1],poolingRatioArray[1]))
    model:add(nn.Mul())
    model:get(4).weight = torch.Tensor(1):fill(poolingRatioArray[1]*poolingRatioArray[1])
    model:add(nn.SoftSign())
    --At this point the values in the model should be the probability of posterior unit activations. I can't find
    --  a function layer that switches values to 1 if values are greater than 0.5 and 0 if less than 0.5, so I'm
    --  going to write a custom module that should accomplish this.

    --Uh, so I have called the new class posteriorProbabilityActivatins that works... for some reason, even though
    --  the require action complains alot.

    model:add(posteriorProbabilityActivations())
    --model:add(nn.Add(???))
    --model:add(nn.SpatialMaxPooling(poolingRatioArray[1],poolingRatioArray[1]) )

    --if more than 1 hidden layer to add we execute the next loop.
    if L > 1 then
        for i = 2,L do
            model:add(nn.SpatialConvolution(filterCountArray[i-1],filterCountArray[i],convFilterSizeArray[i],convFilterSizeArray[i]))
            model:add(nn.Exp())
            model:add(nn.SpatialAveragePooling(poolingRatioArray[i],poolingRatioArray[i],poolingRatioArray[i],poolingRatioArray[i]))
            model:add(nn.Mul())
            --next line sets the weight of the nn.Mul() layer
            model:get(i*6-2).weight = torch.Tensor(1):fill(poolingRatioArray[i]*poolingRatioArray[i])
            model:add(nn.SoftSign())
            model:add(posteriorProbabilityActivations())
            --model:add(nn.SpatialMaxPooling(poolingRatioArray[i],poolingRatioArray[i] ))
        end
    end

    --Going to need to debug this to see correct behaviour, but the dimensions seem right.

    return model
    
end




