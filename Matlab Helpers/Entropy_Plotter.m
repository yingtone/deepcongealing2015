p = linspace(0,1);

y = zeros(1,100);

for i =1:100
   y(i) = -p(i)*log(p(i)) - (1-p(i))*log(1-p(i));
end

y(1) = 0;
y(100) = 0;


%% Now plot!!

plot(p,y)
xlabel('Pooling Unit Stack Average Activation')
ylabel('Entropy Measure H')
axis([0,1,0,0.8])
axis equal