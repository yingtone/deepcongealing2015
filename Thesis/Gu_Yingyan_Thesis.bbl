\begin{thebibliography}{33}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Bell and Sejnowski(1997)]{bell1997independent}
A.~J. Bell and T.~J. Sejnowski.
\newblock The "independent components" of natural scenes are edge filters.
\newblock \emph{Vision research}, 37\penalty0 (23):\penalty0 3327--3338, 1997.

\bibitem[Bengio(2009)]{bengio2009learning}
Y.~Bengio.
\newblock Learning deep architectures for ai.
\newblock \emph{Foundations and trends{\textregistered} in Machine Learning},
  2\penalty0 (1):\penalty0 1--127, 2009.

\bibitem[Bengio et~al.(2007{\natexlab{a}})Bengio, Lamblin, Popovici,
  Larochelle, et~al.]{bengio2007greedy}
Y.~Bengio, P.~Lamblin, D.~Popovici, H.~Larochelle, et~al.
\newblock Greedy layer-wise training of deep networks.
\newblock \emph{Advances in neural information processing systems},
  19:\penalty0 153, 2007{\natexlab{a}}.

\bibitem[Bengio et~al.(2007{\natexlab{b}})Bengio, LeCun,
  et~al.]{bengio2007scaling}
Y.~Bengio, Y.~LeCun, et~al.
\newblock Scaling learning algorithms towards ai.
\newblock \emph{Large-scale kernel machines}, 34\penalty0 (5),
  2007{\natexlab{b}}.

\bibitem[Bengio et~al.(2013)Bengio, Courville, and
  Vincent]{bengio2013representation}
Y.~Bengio, A.~Courville, and P.~Vincent.
\newblock Representation learning: A review and new perspectives.
\newblock \emph{Pattern Analysis and Machine Intelligence, IEEE Transactions
  on}, 35\penalty0 (8):\penalty0 1798--1828, 2013.

\bibitem[Bickerman et~al.(2010)Bickerman, Bosley, Swire, and
  Keller]{bickerman2010learning}
G.~Bickerman, S.~Bosley, P.~Swire, and R.~Keller.
\newblock Learning to create jazz melodies using deep belief nets.
\newblock In \emph{First International Conference on Computational Creativity},
  2010.

\bibitem[Boureau et~al.(2008)Boureau, Cun, et~al.]{boureau2008sparse}
Y.-l. Boureau, Y.~L. Cun, et~al.
\newblock Sparse feature learning for deep belief networks.
\newblock In \emph{Advances in neural information processing systems}, pages
  1185--1192, 2008.

\bibitem[Carreira-Perpinan and Hinton(2005)]{carreira2005contrastive}
M.~A. Carreira-Perpinan and G.~E. Hinton.
\newblock On contrastive divergence learning.
\newblock In \emph{Proceedings of the tenth international workshop on
  artificial intelligence and statistics}, pages 33--40. Citeseer, 2005.

\bibitem[Collobert et~al.()Collobert, Kavukcuoglu, and
  Farabet]{Collobert_torch7:a}
R.~Collobert, K.~Kavukcuoglu, and C.~Farabet.
\newblock Torch7: A matlab-like environment for machine learning.

\bibitem[Erhan et~al.(2009)Erhan, Bengio, Courville, and
  Vincent]{erhan2009visualizing}
D.~Erhan, Y.~Bengio, A.~Courville, and P.~Vincent.
\newblock Visualizing higher-layer features of a deep network.
\newblock \emph{Dept. IRO, Universit{\'e} de Montr{\'e}al, Tech. Rep}, 4323,
  2009.

\bibitem[Erhan et~al.(2010)Erhan, Bengio, Courville, Manzagol, Vincent, and
  Bengio]{erhan2010does}
D.~Erhan, Y.~Bengio, A.~Courville, P.-A. Manzagol, P.~Vincent, and S.~Bengio.
\newblock Why does unsupervised pre-training help deep learning?
\newblock \emph{Journal of Machine Learning Research}, 11:\penalty0 625--660,
  2010.

\bibitem[Fischer and Igel(2012)]{fischer2012introduction}
A.~Fischer and C.~Igel.
\newblock An introduction to restricted boltzmann machines.
\newblock In \emph{Progress in Pattern Recognition, Image Analysis, Computer
  Vision, and Applications}, pages 14--36. Springer, 2012.

\bibitem[Friedman(1987)]{friedman1987exploratory}
J.~H. Friedman.
\newblock Exploratory projection pursuit.
\newblock \emph{Journal of the American statistical association}, 82\penalty0
  (397):\penalty0 249--266, 1987.

\bibitem[Hinton(2010)]{hinton2010practical}
G.~Hinton.
\newblock A practical guide to training restricted boltzmann machines.
\newblock \emph{Momentum}, 9\penalty0 (1):\penalty0 926, 2010.

\bibitem[Hinton(2002)]{hinton2002training}
G.~E. Hinton.
\newblock Training products of experts by minimizing contrastive divergence.
\newblock \emph{Neural computation}, 14\penalty0 (8):\penalty0 1771--1800,
  2002.

\bibitem[Hinton et~al.(2006)Hinton, Osindero, and Teh]{hinton2006fast}
G.~E. Hinton, S.~Osindero, and Y.-W. Teh.
\newblock A fast learning algorithm for deep belief nets.
\newblock \emph{Neural computation}, 18\penalty0 (7):\penalty0 1527--1554,
  2006.

\bibitem[Huang et~al.(2012)Huang, Mattar, Lee, and
  Learned-Miller]{huang2012learning}
G.~Huang, M.~Mattar, H.~Lee, and E.~G. Learned-Miller.
\newblock Learning to align from scratch.
\newblock In \emph{Advances in Neural Information Processing Systems}, pages
  764--772, 2012.

\bibitem[Huang et~al.(2007)Huang, Ramesh, Berg, and Learned-Miller]{LFWTech}
G.~B. Huang, M.~Ramesh, T.~Berg, and E.~Learned-Miller.
\newblock Labeled faces in the wild: A database for studying face recognition
  in unconstrained environments.
\newblock Technical Report 07-49, University of Massachusetts, Amherst, October
  2007.

\bibitem[Krizhevsky et~al.(2012)Krizhevsky, Sutskever, and
  Hinton]{krizhevsky2012imagenet}
A.~Krizhevsky, I.~Sutskever, and G.~E. Hinton.
\newblock Imagenet classification with deep convolutional neural networks.
\newblock In \emph{Advances in neural information processing systems}, pages
  1097--1105, 2012.

\bibitem[Learned-Miller(2006)]{learned2006data}
E.~G. Learned-Miller.
\newblock Data driven image models through continuous joint alignment.
\newblock \emph{Pattern Analysis and Machine Intelligence, IEEE Transactions
  on}, 28\penalty0 (2):\penalty0 236--250, 2006.

\bibitem[Lee et~al.(2008)Lee, Ekanadham, and Ng]{lee2008sparse}
H.~Lee, C.~Ekanadham, and A.~Y. Ng.
\newblock Sparse deep belief net model for visual area v2.
\newblock In \emph{Advances in neural information processing systems}, pages
  873--880, 2008.

\bibitem[Lee et~al.(2009)Lee, Pham, Largman, and Ng]{lee2009unsupervised}
H.~Lee, P.~Pham, Y.~Largman, and A.~Y. Ng.
\newblock Unsupervised feature learning for audio classification using
  convolutional deep belief networks.
\newblock In \emph{Advances in neural information processing systems}, pages
  1096--1104, 2009.

\bibitem[Lee et~al.(2011)Lee, Grosse, Ranganath, and Ng]{lee2011unsupervised}
H.~Lee, R.~Grosse, R.~Ranganath, and A.~Y. Ng.
\newblock Unsupervised learning of hierarchical representations with
  convolutional deep belief networks.
\newblock \emph{Communications of the ACM}, 54\penalty0 (10):\penalty0 95--103,
  2011.

\bibitem[Liao et~al.(2007)Liao, Luo, and Tian]{liao2007whitenedfaces}
L.-Z. Liao, S.-W. Luo, and M.~Tian.
\newblock "whitenedfaces" recognition with pca and ica.
\newblock \emph{Signal Processing Letters, IEEE}, 14\penalty0 (12):\penalty0
  1008--1011, 2007.

\bibitem[Olshausen and Field(1997)]{olshausen1997sparse}
B.~A. Olshausen and D.~J. Field.
\newblock Sparse coding with an overcomplete basis set: A strategy employed by
  v1?
\newblock \emph{Vision research}, 37\penalty0 (23):\penalty0 3311--3325, 1997.

\bibitem[Salakhutdinov(2009)]{salakhutdinov2009learning}
R.~Salakhutdinov.
\newblock \emph{Learning deep generative models}.
\newblock PhD thesis, University of Toronto, 2009.

\bibitem[Salakhutdinov and Hinton(2009)]{salakhutdinov2009deep}
R.~Salakhutdinov and G.~E. Hinton.
\newblock Deep boltzmann machines.
\newblock In \emph{International Conference on Artificial Intelligence and
  Statistics}, pages 448--455, 2009.

\bibitem[Shamir et~al.(2010)Shamir, Sabato, and Tishby]{shamir2010learning}
O.~Shamir, S.~Sabato, and N.~Tishby.
\newblock Learning and generalization with the information bottleneck.
\newblock \emph{Theoretical Computer Science}, 411\penalty0 (29):\penalty0
  2696--2711, 2010.

\bibitem[Tishby and Zaslavsky(2015)]{tishby2015deep}
N.~Tishby and N.~Zaslavsky.
\newblock Deep learning and the information bottleneck principle.
\newblock \emph{arXiv preprint arXiv:1503.02406}, 2015.

\bibitem[Tishby et~al.(2000)Tishby, Pereira, and Bialek]{tishby2000information}
N.~Tishby, F.~C. Pereira, and W.~Bialek.
\newblock The information bottleneck method.
\newblock \emph{arXiv preprint physics/0004057}, 2000.

\bibitem[van~der Schaaf and van Hateren(1996)]{van1996modelling}
v.~A. van~der Schaaf and J.~v. van Hateren.
\newblock Modelling the power spectra of natural images: statistics and
  information.
\newblock \emph{Vision research}, 36\penalty0 (17):\penalty0 2759--2770, 1996.

\bibitem[Yosinski and Lipson(2012)]{yosinski2012visually}
J.~Yosinski and H.~Lipson.
\newblock Visually debugging restricted boltzmann machine training with a 3d
  example.
\newblock In \emph{Representation Learning Workshop, 29th International
  Conference on Machine Learning}, 2012.

\bibitem[Zitova and Flusser(2003)]{zitova2003image}
B.~Zitova and J.~Flusser.
\newblock Image registration methods: a survey.
\newblock \emph{Image and vision computing}, 21\penalty0 (11):\penalty0
  977--1000, 2003.

\end{thebibliography}
