\select@language {UKenglish}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Image Registration}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Unsupervised Feature Learning}{7}{section.1.2}
\contentsline {chapter}{\numberline {2}Preliminaries}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Restricted Boltzmann Machines}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Probability Densities and Conditional Distributions}{9}{subsection.2.1.1}
\contentsline {subsubsection}{Binary-Valued Visible Nodes}{9}{section*.4}
\contentsline {subsubsection}{Real-Valued Visible Nodes}{10}{section*.5}
\contentsline {section}{\numberline {2.2}Convolutional Restricted Boltzmann Machines}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Probability Densities and Conditional Distributions}{12}{subsection.2.2.1}
\contentsline {subsubsection}{Binary Valued Visible Nodes}{12}{section*.6}
\contentsline {subsubsection}{Real Valued Visible Nodes}{12}{section*.7}
\contentsline {subsection}{\numberline {2.2.2}Introducing Probabilistic max-pooling}{13}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Contrastive Divergence - Approximating the Likelihood Gradient}{14}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Inducing Sparsity}{15}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Training}{16}{subsection.2.3.2}
\contentsline {subsubsection}{CRBM Training Algorithm}{16}{section*.8}
\contentsline {subsubsection}{Notes on the CRBM Training Algorithm}{17}{section*.9}
\contentsline {section}{\numberline {2.4}Deep Belief Networks}{18}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Training Deep Belief Networks}{19}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Image Registration via Deep Congealing}{19}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}The Congealing Framework}{20}{subsection.2.5.1}
\contentsline {subsubsection}{Congealing Algorithm}{20}{section*.10}
\contentsline {subsection}{\numberline {2.5.2}Incorporating Feature Learning}{21}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Whitening}{22}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Discrete Fourier Transforms and Flattening Power Spectra}{23}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Image-Class Specific Whitening}{24}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Approaching Image Alignment from an Information Theory perspective}{25}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}The Information Bottleneck Principle}{26}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Deep Neural Networks and the Information Bottleneck}{27}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}A Proposed Extension; All-Layer Congealing with Adaptive Whitening}{28}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Considering Different Depths of Convolutional Deep Belief Network}{28}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}All-Layer Congealing; Considering all the depths of the CDBN}{29}{subsection.2.8.2}
\contentsline {subsection}{\numberline {2.8.3}Arguments For and Against All-Layer Congealing}{29}{subsection.2.8.3}
\contentsline {subsection}{\numberline {2.8.4}On Adaptive Whitening}{30}{subsection.2.8.4}
\contentsline {chapter}{\numberline {3}Experiments}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}Torch, lua and the nn package}{31}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}The Data and the Model}{32}{subsection.3.1.1}
\contentsline {subsubsection}{The CDBN Model}{32}{section*.11}
\contentsline {subsubsection}{Generating the Hidden Layer to be used as the Visible Layer to continue Layer-Wise Training.}{33}{section*.12}
\contentsline {subsubsection}{Performing Convolutions and Pooling}{33}{section*.13}
\contentsline {subsection}{\numberline {3.1.2}The Parameters; Initialising and Learning}{33}{subsection.3.1.2}
\contentsline {subsubsection}{Initialising the Parameters}{34}{section*.14}
\contentsline {subsubsection}{Learning the parameters}{34}{section*.15}
\contentsline {subsubsection}{Momentum and Weight-Decay}{35}{section*.16}
\contentsline {subsection}{\numberline {3.1.3}Statistics to Collect and Expected Behaviour}{36}{subsection.3.1.3}
\contentsline {subsubsection}{Patch Updates; or the Detriment of Speed v.s. Completeness}{37}{section*.17}
\contentsline {subsection}{\numberline {3.1.4}Learning Filters, Visualising Filters and Training Results}{37}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Implementing Congealing}{38}{section.3.2}
\contentsline {chapter}{\numberline {4}Discussion}{40}{chapter.4}
\contentsline {subsection}{\numberline {4.0.1}Generating New Images}{40}{subsection.4.0.1}
\contentsline {subsection}{\numberline {4.0.2}Concerning the 3rd Dimension}{40}{subsection.4.0.2}
\contentsline {subsection}{\numberline {4.0.3}Combining Generative Power with the Congealing Framework}{40}{subsection.4.0.3}
\contentsline {subsection}{\numberline {4.0.4}Applying other image registration techniques to the deepest layer of the CDBN}{40}{subsection.4.0.4}
\contentsline {chapter}{\numberline {5}Conclusion}{42}{chapter.5}
\contentsline {chapter}{Bibliography}{43}{section*.18}
\contentsline {chapter}{\numberline {A}Appendix}{46}{appendix.A}
\contentsline {section}{\numberline {A.1}Whitening; A Biological Perspective}{46}{section.A.1}
\contentsline {section}{\numberline {A.2}Torch Tricks}{46}{section.A.2}
\contentsline {section}{\numberline {A.3}Folder Layout and general Information Flow}{46}{section.A.3}
\contentsline {section}{\numberline {A.4}Code}{46}{section.A.4}
\contentsline {chapter}{\numberline {B}More Guidelines}{47}{appendix.B}
\contentsline {section}{\numberline {B.1}Titlepage}{47}{section.B.1}
\contentsline {section}{\numberline {B.2}Declaration}{47}{section.B.2}
