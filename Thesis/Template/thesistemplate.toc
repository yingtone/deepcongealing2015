\select@language {UKenglish}
\contentsline {chapter}{\numberline {1}This might be some kind of Introduction or Literature Review}{6}{chapter.1}
\contentsline {chapter}{\numberline {2}Then you get to the meaty stuff}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}With things like Methods}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}organised into subsections if you want}{7}{subsection.2.1.1}
\contentsline {subsubsection}{or even subsubsections}{7}{section*.4}
\contentsline {chapter}{\numberline {3}And yet more meaty stuff}{8}{chapter.3}
\contentsline {chapter}{\numberline {4}Conclusion}{10}{chapter.4}
\contentsline {section}{\numberline {4.1}About Referencing}{10}{section.4.1}
\contentsline {chapter}{Bibliography}{11}{section*.5}
\contentsline {chapter}{\numberline {A}You probably want to put things like computer programs and long boring proofs in an appendix}{12}{appendix.A}
\contentsline {section}{\numberline {A.1}R code}{12}{section.A.1}
\contentsline {chapter}{\numberline {B}More Guidelines}{13}{appendix.B}
\contentsline {section}{\numberline {B.1}Titlepage}{13}{section.B.1}
\contentsline {section}{\numberline {B.2}Declaration}{13}{section.B.2}
