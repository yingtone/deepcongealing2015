require 'nn'

do

	local postProbActivations = torch.class('posteriorProbabilityActivations', 'nn.Module')

	function postProbActivations:updateOutput(input)
	input[torch.le(input,0.5)] = 0
	input[torch.ge(input,0.5)] = 1
	self.output = input
	return self.output
	end

	function postProbActivations:updateGradInput(input, gradOutput)
	return self.gradInput
	end

end
