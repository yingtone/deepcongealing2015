
-- require 'nn'
-- require 'image' --necessary?
require("MCMCUpdatesFunctions")
require("dataset_filename_retriever")
require("parameterInitialiserFunction")
require("mega_batch_loader")
require("reconstruction_error")
require("filterWeights_average_multiplication_mask")
require("graph_and_histogram_saver")
require("parameter_saver")
require("hidden_activation_image_saver")
--This is the version 

function CDBNLayerTrainer2(model,layer_number,unique_Folder_Location,learningRate,sparsityPenalty,sparsityRate,K,Vl,maxRepeat,pooling_ratio_array)

    --learningRate a 3 large array, first loc the filterWeights learningRate, second the hiddenBias,
    --  third the visBias.

    --variable setup---
    local convergenceFlag = 0
    local repeatCounter = 0
    local inner_while_repeat_counter = 0
    local index = (layer_number-1)*5 + 1
    local Nw = math.sqrt(model:get(index).weight:size()[2]/Vl)
    local batch_number = 0
    local batch_complete_flag = 0
    local mini_batch_number = 0
    local number_of_minibatches = 0
    local mini_batch_size = 10
    local mini_batch_act_size = 0
    local mini_batch_recon_error = 0
    local Nh = 0
    --local sigma
    local sigma =1
    local sigma_start = 0.2w
    local sigma_stop = 0.1
    if(layer_number == 1) then
        sigma = sigma_start
    end
    local initial_momentum = 0.5
    local final_momentum = 0.9
    local momentum = initial_momentum

    local filterWeights_addition
    local sparsityPenaltyAddition 
    local hiddenBias_addition
    local visBias_addition

    local dynamic_learning_change_array = torch.Tensor(10):fill(0)
    local dynamic_learn_flag = 0
    local dynamic_repeat_counter = -1
    local prev_recon_error = 0
    local mini_batch_cumulant_error = 0
    --/variable setup---
    
    --diagnostic variable setup
    local graph_inner_repeat_epoch_number = 100*100-- sets after how many inner while executions to generate graphs and histograms
    local graph_number = 0 --catalogues which overall set of graphs we are calculating
    local filterWeights_mask = filterWeights_average_multiplication_mask(Vl,Nw)
    local filterWeights_heatmap -- calculates the average movement for each filterweight
    local filterWeights_directionality -- calculates the average movement for each filterweight
    local filterWeights_average_histogram -- saves what the average value for each filter is
    local hiddenBias_graph 
    local hiddenBias_increments_histogram 
    local visBias_graph
    local visBias_increments_histogram 
    local reconstruction_error_graph
    local hidden_activation_graph
    local startTime = os.time()
    local midTime
    local batch_to_observe 
    local batch_to_observe_counter = 0
    local batch_to_observe_flag = 0
    --/diagnostic variable setup

    --LOADING STEP
    --number_of_batches is how many .tns files we have as mega batches, filenameTable gives us their locations
    print("Attempting dataset_filename_retriever")
    local number_of_batches, filename_Table = dataset_filename_retriever(model,layer_number,unique_Folder_Location)
    --random_order gives the order of considering each subsequent megabatch to introduce randomness. It was very hard to do this quickly within each megabatch
    print(filename_Table)
    --Loading the data
    local mega_batch_size, Nv, mega_batch = mega_batch_loader(filename_Table, 1)
    print("mega_batch loaded from: " .. filename_Table[1])
    Nh = Nv-Nw+1
    --/LOADING STEP

    --INITIALISATION STEP. 
    local filterWeights,hiddenBias,visBias = parameterInitialiser(K,Vl,Nw,pooling_ratio_array[layer_number],sparsityPenalty,mega_batch, sigma) 

    --Write Initial Learning Summary File file
    file = io.open(unique_Folder_Location.."/Diagnostics/Layer_".. layer_number .. "/Learning_Summary_File.txt","w")
    file:write("Training a model with " .. K .. " hidden layers with " .. Vl .." visible layers." .."\n")
    file:write("Training commenced on " .. os.date("%c",current_time) .."\n \n")
    file:close()  --
    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number)
    --/INITIALISATION STEP.



    -- Specific to this Trainer variables
    local mini_batch
    local patch_sizer = 1
    if(layer_number > 1) then
        patch_sizer = pooling_ratio_array[layer_number]*(layer_number-1)
    end
    local patch_Nv = math.min(Nv,50)
    local patch_Nh = patch_Nv - Nw + 1
    --adjusting image size so we don't have to do any padding on the hidden update generation
    patch_Nh = math.floor(patch_Nh/pooling_ratio_array[layer_number] )*pooling_ratio_array[layer_number] 
    patch_Nv = patch_Nh + Nw - 1
    local patch_batch_size = 2
    mini_batch_size = math.floor(500/patch_batch_size)
    --/SttTv
    local gradFilterConv = nn.SpatialConvolution(1,K,patch_Nh,patch_Nh)
    local learn_signal_exponential_convolutional = nn.SpatialConvolutionMM(Vl,K,Nw,Nw)

    print("Starting repeat loop")
    while (repeatCounter < maxRepeat and convergenceFlag == 0) do
        --randomly pull out 100 images to work on
        repeatCounter = repeatCounter + 1
        print(" Progress repeatCounter: " .. repeatCounter .."/" ..maxRepeat)
        local shuffle = torch.randperm(mini_batch_size)
        if(repeatCounter>5) then
            momentum = final_momentum
        end
        for i =1,mini_batch_size do 
            local image_to_consider = torch.Tensor(1,Vl,Nv,Nv)
            image_to_consider = mega_batch[{{shuffle[i]}}]:clone()

            for j = 1,patch_batch_size do 
                --Pull out a patch
                local xStartIndex = math.ceil( torch.rand(1)[1]*(Nv + 1- patch_Nv))
                local yStartIndex = math.ceil(torch.rand(1)[1]*(Nv + 1- patch_Nv))
                local patch = torch.Tensor(1,Vl,patch_Nv,patch_Nv)
                patch = image_to_consider[{{},{},{xStartIndex,xStartIndex + patch_Nv-1},{yStartIndex,yStartIndex + patch_Nv-1}}]
                
                if (layer_number == 1) then
                    patch = patch - patch:mean() --Assuming zero visible bias on the first layer only. 
                end

                --flip patch?
                if(math.random() > 0.5 and layer_number==1) then
                    patch = image.flip(patch,4) --equivalent to flipping the image left-to-right - assume face symmetry
                end

                --setting up the graph_number^th graph storage variables AND FILENAMES and FOLDER LOCATIONS to save into
                if(inner_while_repeat_counter % graph_inner_repeat_epoch_number == 0) then 
                    graph_number = graph_number + 1
                    print("New epoch!")
                    filterWeights_heatmap = torch.Tensor(filterWeights:size()):zero() -- calculates the average movement for each filterweight
                    filterWeights_directionality = torch.Tensor(filterWeights:size()):zero() -- calculates the average movement for each filterweight
                    filterWeights_average_histogram = torch.Tensor(graph_inner_repeat_epoch_number,K,Vl):zero() -- saves what the average value for each filter is
                    hiddenBias_graph = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
                    hiddenBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
                    visBias_graph = torch.Tensor(graph_inner_repeat_epoch_number, Vl):zero()
                    visBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number, Vl):zero()
                    reconstruction_error_graph = torch.Tensor(graph_inner_repeat_epoch_number):zero()
                    hidden_activation_graph = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
                end
                --save filterWeights copies after every (large number of epochs) ??
                inner_while_repeat_counter = inner_while_repeat_counter + 1


                --Breals somewhere between here ...
                --print("Attempting CD Updates")i

                learn_signal_exponential_convolutional.weight = filterWeights/(sigma^2)
                learn_signal_exponential_convolutional.bias = hiddenBias/(sigma^2)
                local positive_phase_learn_signal = learn_signal_exponential_convolutional:forward(patch)

                local positive_hidden_sample, positive_hidden_probabilities =  gibbsHiddenUpdate(patch_Nh,K,pooling_ratio_array[layer_number],1,positive_phase_learn_signal)

                --print("Check1")
                local patch_generated = torch.Tensor(patch:size())            
                
    	        --Doing CD-1
    	        for j = 1,1 do
                	if index == 1 then
                	    -- Rmini_batch_cloneeal Visible Layer 
                	    patch_generated = gibbsRealVisibleUpdate(filterWeights,patch_Nv,K,Vl,Nw,1,visBias,positive_hidden_sample)
                	else 
                	    -- Binary Visible Layer, since we always use the probabilties practically when sampling 
                	    --mini_batch_generated, prob_Visible_Tensor = gibbsBinaryVisibleUpdate(model,index,Nv,K,Vl,Nw,visBias,hiddenTensor)
                	    patch_generated = gibbsBinaryVisibleUpdate(filterWeights,patch_Nv,K,Vl,Nw,1,visBias,positive_hidden_sample)
                	end

    	    	--Adding variance for the real RBM case
                	local negative_phase_learn_signal = learn_signal_exponential_convolutional:forward(patch_generated)
                    --Naming of positive_hidden_sample not technically correct
                	positive_hidden_sample, negative_hidden_probabilities =  gibbsHiddenUpdate(patch_Nh,K,pooling_ratio_array[layer_number],1,negative_phase_learn_signal)
                end
                --/MCMC UPDATES STEP.

                --print("Attempting Parameter Update convolutions")

                gradFilterConv.bias = torch.Tensor(K):fill(0)
                local origSampleFilterWeightDifferential = torch.Tensor(K,Vl,Nw,Nw):zero() 
                local newSampleFWDiff = torch.Tensor(K,Vl,Nw,Nw):zero()
                gradFilterConv.weight[{{},{1},{},{}}] = positive_hidden_probabilities[1]
                for l = 1,Vl do
                    origSampleFilterWeightDifferential[{{},l ,{} ,{}}] = origSampleFilterWeightDifferential[{{},l ,{} ,{}}] + gradFilterConv:forward(patch[{1,{l},{},{}}])
                end 
                gradFilterConv.weight[{{},{1},{},{}}] = negative_hidden_probabilities[1]
                for l = 1,Vl do 
                  newSampleFWDiff[{{},l ,{} ,{}}] = newSampleFWDiff[{{},l ,{} ,{}}] + gradFilterConv:forward(patch_generated[{1,{l},{},{}}]) 
                end  


                --average hidden activation
                for r = 1,K do 
                    hidden_activation_graph[{inner_while_repeat_counter,r}] = positive_hidden_probabilities[{{},r }]:mean()
                end
                print("Average Hidden Activation: " .. positive_hidden_probabilities:mean())

                if(filterWeights_addition == nil ) then
                    --haven't been initialised yet, can't do momentum just yet
                    filterWeights_addition = (origSampleFilterWeightDifferential:add(-1,newSampleFWDiff)/(patch_Nh*patch_Nh)):mul(learningRate[1])
                    sparsityPenaltyAddition = torch.Tensor(K):fill(sparsityPenalty) - positive_hidden_probabilities:sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nh*patch_Nh)
                    hiddenBias_addition = ((positive_hidden_probabilities:add(-1,negative_hidden_probabilities):sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nh*patch_Nh)):add(sparsityRate,sparsityPenaltyAddition)):mul(learningRate[2])
                    visBias_addition = ((patch-patch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nv*patch_Nv)):mul(learningRate[3])
                else
                    --updates have been initialised; can do momentum updates
                    filterWeights_addition:mul(momentum):add(learningRate[1],(origSampleFilterWeightDifferential:add(-1,newSampleFWDiff)/(patch_Nh*patch_Nh)))
                    sparsityPenaltyAddition = torch.Tensor(K):fill(sparsityPenalty) - positive_hidden_probabilities:sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nh*patch_Nh)
                    hiddenBias_addition:mul(momentum):add(learningRate[2],((positive_hidden_probabilities:add(-1,negative_hidden_probabilities):sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nh*patch_Nh)):add(sparsityRate,sparsityPenaltyAddition))    )
                    visBias_addition:mul(momentum):add(learningRate[3],((patch-patch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(patch_Nv*patch_Nv)))                
                end--updates have been initialised; can do momentum updates

                --Suppress output once done
--                print(filterWeights_addition[1])
                
                --Selective decay
    	        --if(filterWeights:var() > 0.0005) then
                local filterWeightsDecay
                if(layer_number == 1) then
                    filterWeightsDecay = filterWeights*0.01 -- Equivalent to the weight decay
                else
                    filterWeightsDecay = filterWeights*0.0001
                end
                --end

--    	        print(torch.abs(filterWeights_addition):mean()*learningRate[1] .." is average filterWeight_additon*learningRate[1]")
                --print("parameter updates step")
                filterWeights:add(1, torch.reshape(filterWeights_addition:contiguous(),K,Vl*Nw*Nw) ):add(-learningRate[1],filterWeightsDecay)
                hiddenBias:add(1,hiddenBias_addition)
                visBias:add(1,visBias_addition)
    	    
                print("filterWeights min:" .. filterWeights:min())
--    	        print("filterWeights mean:" .. filterWeights:mean())

                --Calculate diagnostics
                mini_batch_recon_error = reconstruction_error(patch,patch_generated)
                ---print recon error --------------------------------------------------------------
                mini_batch_cumulant_error = mini_batch_cumulant_error + mini_batch_recon_error	    
                reconstruction_error_graph[inner_while_repeat_counter] = mini_batch_recon_error
    	        filterWeights_average_histogram[{inner_while_repeat_counter,{},{}}]:addmm(filterWeights, filterWeights_mask/(Nw*Nw))--some type of matrix multiplication to go here
                hiddenBias_graph[inner_while_repeat_counter] = hiddenBias
                hiddenBias_increments_histogram[inner_while_repeat_counter] = hiddenBias_addition*learningRate[2]
                visBias_graph[inner_while_repeat_counter] = visBias
                visBias_increments_histogram[inner_while_repeat_counter] = visBias_addition*learningRate[3]


                --At this point we would have just finished collecting the statistics - now need to save them.
                if(inner_while_repeat_counter % graph_inner_repeat_epoch_number == 0) then
                    print ("End of inner epoch " .. graph_number ..", attempting parameter_saver")
                    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number,graph_number)
    		
                    file = io.open(unique_Folder_Location.."/Diagnostics/Layer_".. layer_number .. "/Learning_Summary_File.txt","a")
                    file:write("End of epoch " ..graph_number .." has reconstruction_error of:" ..mini_batch_recon_error.."\n")
                    file:close()  --
                    print("Attempting graph generation And Saving.")
                    if(pcall(graph_and_histogram_saver(filterWeights_average_histogram,filterWeights_heatmap,filterWeights_directionality,hiddenBias_graph,hiddenBias_increments_histogram,visBias_graph,visBias_increments_histogram,reconstruction_error_graph,unique_Folder_Location, repeatCounter, layer_number,graph_number,Vl,K,Nw,hidden_activation_graph))) then
                        print("Graph Generation Success!!!")
                    else
                        print("Graph generation failure...")
                    end
                    
                    --Saving and gnuplotting!
    		         inner_while_repeat_counter = 0
                end
                --print("end inner while loop")
                midTime = os.time()
            end
        end
        collectgarbage("collect")
        -- Diagnostic Monitors, histograms and such
        --  free Energy, reconstruction error
        print("Time Elapsed: " .. (midTime - startTime))
	    print("Total error of mega_batch is: " .. mini_batch_cumulant_error)
        dynamic_repeat_counter = dynamic_repeat_counter + 1
        print("learningRate is: " ..learningRate[1] .. ", " .. learningRate[2] .. ", " .. learningRate[3] )
    	print("Average hiddenBias is: " ..hiddenBias:mean())
    	print("Average FilterWeight is: " .. filterWeights:mean())
    	print("Average VisBias is: " .. visBias:mean())
	    print("filterWeight_addition[1]:")
	    print(filterWeights_addition[1])

        mini_batch_cumulant_error = 0
        batch_to_observe_flag = 0

        --sigma_decay
        if(sigma > sigma_stop and layer_number==1) then
            sigma = sigma * 0.995
        end
        print("Sigma: " .. sigma)
	    print("end outer while loop")
    end
    
    --set repeatCounter to -1; indicates finishing
    --At this point the weights and hiddenBias and visBias should have been learned.
    repeatCounter = -1
    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number)
        --Saves what the parameters actually are, and does not save sigma.
    return filterWeights, hiddenBias, filename_Table
end

