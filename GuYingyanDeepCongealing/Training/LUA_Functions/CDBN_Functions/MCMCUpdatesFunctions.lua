
-- require 'nn'
-- require 'image' --necessary?

-- inputs: the Model itself, the index of the layer in question.
--   How to handle the data-set? We don't want too much memory copy, so I propose that in the training of the 
--   model we pass handles to a clone of the training data-set of that particular layer. The function should
--   then change the contents of the storage that that training data-set refers to.

-- visible given hidden, two functions, realVisible and binaryVisible. The number of visibleLayers shouldn't 
--   change
--[[
function gibbsRealVisibleUpdate(model,index,Nv,K,Vl,Nw,NumBatch,visBias,hiddenNodeTensor,sigma)
  -- cap proposals? Currently it can propose visible values outside 0 and 1 which doesn't agree with the orig. image
  --Expects hiddenNodeTensor to be a NumBatchxKxNhxNh tensor
  local visConv = nn.SpatialConvolutionMM(K,Vl,Nw,Nw,1,1,Nw-1,Nw-1) 
  local temp = image.flip(torch.reshape(model:get(index).weight,(K*Vl),(Nw*Nw)),2)
  for i = 1,K do --2=K
    visConv.weight[{{},{(i-1)*(Nw*Nw)+1,i*Nw*Nw}}] = temp[{{(i-1)*(Vl)+1,i*(Vl)},{}}] --4 = Nw*Nw
  end
  visConv.bias = visBias/sigma
  return visConv:forward(hiddenNodeTensor)*sigma -- + torch.randn(NumBatch,Vl,Nv,Nv)*sigma
end
]]--

function gibbsRealVisibleUpdate(filterWeights,Nv,K,Vl,Nw,NumBatch,visBias,hiddenNodeTensor)
  -- cap proposals? Currently it can propose visible values outside 0 and 1 which doesn't agree with the orig. image
  --Expects hiddenNodeTensor to be a NumBatchxKxNhxNh tensor
  local visConv = nn.SpatialConvolutionMM(K,Vl,Nw,Nw,1,1,Nw-1,Nw-1) 
--i  print(model:get(index).weight[{1,{1,10}}])
  local temp = image.flip(torch.reshape(filterWeights,(K*Vl),(Nw*Nw)),2)
  for i = 1,K do --2=K
    visConv.weight[{{},{(i-1)*(Nw*Nw)+1,i*Nw*Nw}}] = temp[{{(i-1)*(Vl)+1,i*(Vl)},{}}] --4 = Nw*Nw
  end
  visConv.bias = visBias
  return visConv:forward(hiddenNodeTensor) -- + torch.randn(NumBatch,Vl,Nv,Nv)*sigma
end

function gibbsBinaryVisibleUpdate(filterWeights,Nv,K,Vl,Nw,NumBatch,visBias,hiddenNodeTensor)
  --Expects hiddenNodeTensor to be a NumBatchxKxNhxNh tensor 
  local visConv = nn.SpatialConvolutionMM(K,Vl,Nw,Nw,1,1,Nw-1,Nw-1) 
  --  Need to Import filter parameters here. Need to swap first and second dimensions (input to output planes)
  local temp = image.flip(torch.reshape(filterWeights,K*Vl,Nw*Nw),2)
  for i = 1,K do --2=K
    visConv.weight[{{},{(i-1)*(Nw*Nw)+1,i*Nw*Nw}}] = temp[{{(i-1)*(Vl)+1,i*(Vl)},{}}] --4 = Nw*Nw
  end
  visConv.bias = visBias
  --meanTensor = visConv:forward(hiddenNodeTensor)
  local probVTensor = nn.Sigmoid():forward(visConv:forward(hiddenNodeTensor)) --Gets the probability of visible node being active
  --unifProbs = torch.rand(Vl,Nv,Nv) --Needs to be a numVisPlanesxNvxNv uniform probability tensor
  --visibleNodeTensor = torch.le(unifProbs,probTensor):double()
  --return torch.le(torch.rand(NumBatch,Vl,Nv,Nv),probVTensor):double()
  --practically we won't ever use the sample visible data in the binary case - geoff hinton in his practical guide to rbms
  return probVTensor
end

function gibbsHiddenUpdate(Nh,K,poolingRatio,NumBatch,hiddenNodeTensor)
  --Expects hiddenNodeTensor to be a NumBatchxKxNhxNh tensor   
  local numeTensor = hiddenNodeTensor:exp() --assuming that hiddenNodeTensor has already been calculated.
    --Want to keep this function as fast as possible. If needed though can pull the model into this function
  local sumDenomConv = nn.SpatialConvolutionMM(1,1,poolingRatio,poolingRatio,poolingRatio,poolingRatio)
  sumDenomConv.weight:fill(1) --After each forward we have the inputs for the i'th filter as per
--    the for loop
  sumDenomConv.bias:fill(1)
  local sumNumeTensor = nn.SpatialZeroPadding(0,poolingRatio-Nh%poolingRatio,0,poolingRatio-Nh%poolingRatio):forward(numeTensor) -- pads so that we get
    --proper addition of all possible hidden node values; getting around SpatialConvolution quirks.
    --Also SpatialZeroPadding still works with the 4D tensors!
  local denomTensor = torch.Tensor(sumNumeTensor:size())

  --At this point sumNumeTensor should be a numBatchxKxNh+??xNh+??.
  --  So, when we iterate through the next for loop we iterate through the SECOND dimension. spatConvMM can do batch processing

  for i = 1,K do
  --first for loop loops through the hidden loop
      local contractDenomTensor = sumDenomConv:forward(sumNumeTensor[{{},{i},{},{}}]) -- is a 1x(Nh/poolingRatio)x(Nh/poolingRatio) tensor
        --Below expands out the tensor to the correct size
      denomTensor[{{},{i},{},{}}] = sumDenomConv:backward(sumNumeTensor[{{},{i},{},{}}],contractDenomTensor)
  end

  --set elements in denomTensor from 0 to 1 - avoids nans in the probTensor when we cdiv, i.e. divide by zero
  denomTensor[torch.eq(denomTensor,0)] = 1

  local probTensor = numeTensor:cdiv(denomTensor[{{},{},{1,Nh},{1,Nh}}])
  --unifProbs = torch.rand(K,Nh,Nh)
  --hiddenNodeTensor = torch.le(unifProbs,probTensor):double() --This bit activates the relevant units.
  return torch.le(torch.rand(NumBatch,K,Nh,Nh),probTensor):double(), probTensor
end

