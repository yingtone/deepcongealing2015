function parameterInitialiser(K,Vl,Nw,p,targetSparsity,mega_batch,sigma)
    --p the pooling ratio
    --FW a Kx(VlxNwxNw) tensor (i.e. 2D, since generating weights for the spatConvMM paradigm)
    --initBias a K large tensor
    --vB a Vl large tensor
--[[
    local FW = torch.randn(K,(Vl*Nw*Nw))/50 --Want a standard deviation of around 0.01
    local initBias = torch.randn(K)/80 -- + torch.Tensor(K):fill(torch.log(targetSparsity/(1-p*p*targetSparsity)))
    local vB = torch.Tensor(Vl):fill(0)
    --  Extension - set bias of vis unit to log(p_i/(1-p_i)), p_i the average value of that visible unit
    return FW, initBias, vB
]]--

    --mega_batch:size() = (NumImages,Vl,Nv,Nv)
    local visMean = torch.Tensor(Vl)
    local visVar = torch.Tensor(Vl)
    for i = 1,Vl do 
        visMean[i] = mega_batch[{{},i}]:mean()
        visVar[i] = mega_batch[{{},i}]:var()
    end
    local FW = torch.Tensor(K,(Vl*Nw*Nw))
    for i = 1,Vl do 
        FW[{{}, {(i-1)*Nw*Nw+1, i*Nw*Nw} }] = torch.randn(K, Nw*Nw)*math.sqrt(visVar[i])*math.sqrt(0.001)
    end
    local initBias = torch.Tensor(K):fill(torch.log(targetSparsity/(1-p*p*targetSparsity)))*(sigma^2)
    local vB = torch.Tensor(Vl)
    
    --
    if(Vl==1) then
        vB[1] = visMean[1]
        --Assuming this is the real visible case
    else
        for i =1,Vl do
            vB[i] = math.log(visMean[i]/(1-visMean[i]))
        end
    end
    --  Extension - set bias of vis unit to log(p_i/(1-p_i)), p_i the average value of that visible unit
    return FW, initBias, vB
end
