function filterWeights_average_multiplication_mask(Vl,Nw)
	local returnTensor = torch.Tensor(Vl*Nw*Nw,Vl):zero()
	for i = 1,Vl do
		returnTensor[{{ (i-1)*Nw*Nw+1, i*Nw*Nw },i}]:fill(1)
	end
	return returnTensor
end