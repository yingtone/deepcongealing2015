
-- require 'nn'
-- require 'image' --necessary?
require("MCMCUpdatesFunctions")
require("dataset_filename_retriever")
require("parameterInitialiserFunction")
require("mega_batch_loader")
require("reconstruction_error")
require("filterWeights_average_multiplication_mask")
require("graph_and_histogram_saver")
require("parameter_saver")
require("hidden_activation_image_saver")
--This is the version 

function CDBNLayerTrainer(model,layer_number,unique_Folder_Location,learningRate,sparsityPenalty,sparsityRate,K,Vl,maxRepeat,pooling_ratio_array)

    --learningRate a 3 large array, first loc the filterWeights learningRate, second the hiddenBias,
    --  third the visBias.

    --variable setup---
    local convergenceFlag = 0
    local repeatCounter = 0
    local inner_while_repeat_counter = 0
    local index = (layer_number-1)*5 + 1
    local Nw = math.sqrt(model:get(index).weight:size()[2]/Vl)
    local batch_number = 0
    local batch_complete_flag = 0
    local mini_batch_number = 0
    local number_of_minibatches = 0
    local mini_batch_size = 10
    local mini_batch_act_size = 0
    local mini_batch_recon_error = 0
    local Nh = 0
    --local sigma
    local sigma =1
    local sigma_start = 0.2
    local sigma_stop = 0.1

    local initial_momentum = 0.5
    local final_momentum = 0.9
    local momentum = initial_momentum

    local filterWeights_addition
    local sparsityPenaltyAddition 
    local hiddenBias_addition
    local visBias_addition

    local dynamic_learning_change_array = torch.Tensor(10):fill(0)
    local dynamic_learn_flag = 0
    local dynamic_repeat_counter = -1
    local prev_recon_error = 0
    local mini_batch_cumulant_error = 0
    --/variable setup---
    
    --diagnostic variable setup
    local graph_inner_repeat_epoch_number = 1*50 -- sets after how many inner while executions to generate graphs and histograms
    local graph_number = 0 --catalogues which overall set of graphs we are calculating
    local filterWeights_mask = filterWeights_average_multiplication_mask(Vl,Nw)
    local filterWeights_heatmap -- calculates the average movement for each filterweight
    local filterWeights_directionality -- calculates the average movement for each filterweight
    local filterWeights_average_histogram -- saves what the average value for each filter is
    local hiddenBias_graph 
    local hiddenBias_increments_histogram 
    local visBias_graph
    local visBias_increments_histogram 
    local reconstruction_error_graph
    local hidden_activation_graph
    local startTime = os.time()
    local midTime
    local batch_to_observe 
    local batch_to_observe_counter = 0
    local batch_to_observe_flag = 0
    --/diagnostic variable setup
-- OLD INITIALISATION STEP WITH PARAMETERINITIALISER
    --INITIALISATION STEP. 
    local filterWeights,hiddenBias,visBias = parameterInitialiser(K,Vl,Nw,pooling_ratio_array[layer_number],sparsityPenalty) 

--[[
    --Trying out some pre-learned filters from earlier
    if(layer_number==1) then
    testReadFile = torch.DiskFile('./LUA_Functions/CDBN_Functions/copiedParams/filterWeightsCopy2pN.foo', 'r')
    filterWeightsRead = testReadFile:readObject()
    for i = 1,K do
	filterWeights[{i}] = torch.reshape(filterWeightsRead[i],Nw*Nw) 
    end
    --testReadFile = torch.DiskFile('./LUA_Functions/CDBN_Functions/copiedParams/hiddenBiasCopy2pN.foo', 'r')
    --hiddenBias = testReadFile:readObject()
    testReadFile = torch.DiskFile('./LUA_Functions/CDBN_Functions/copiedParams/visBiasCopy2pN.foo', 'r')
    visBias = testReadFile:readObject()
    end
]]--
    --Write Initial Learning Summary File file
    file = io.open(unique_Folder_Location.."/Diagnostics/Layer_".. layer_number .. "/Learning_Summary_File.txt","w")
    file:write("Training a model with " .. K .. " hidden layers with " .. Vl .." visible layers." .."\n")
    file:write("Training commenced on " .. os.date("%c",current_time) .."\n \n")
    file:close()  --
    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number)

    --links the model parameters to the parameters being iterated upon
    model:get(index).weight = filterWeights
    model:get(index).bias = hiddenBias
    --/INITIALISATION STEP.
--[[
    --INITIALISATION STEP. 
    local visBias = torch.Tensor(Vl):fill(0)
    --Write Initial Learning Summary File file
    file = io.open(unique_Folder_Location.."/Diagnostics/Layer_".. layer_number .. "/Learning_Summary_File.txt","w")
    file:write("Training a model with " .. K .. " hidden layers with " .. Vl .." visible layers." .."\n")
    file:write("Training commenced on " .. os.date("%c",current_time) .."\n \n")
    file:close()  --
    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number)

    --links the model parameters to the parameters being iterated upon
    filterWeights = model:get(index).weight
    hiddenBias = model:get(index).bias
    hiddenBias:fill(0)
    --/INITIALISATION STEP.
]]--

    --number_of_batches is how many .tns files we have as mega batches, filenameTable gives us their locations
    print("Attempting dataset_filename_retriever")
    local number_of_batches, filename_Table = dataset_filename_retriever(model,layer_number,unique_Folder_Location)
    --random_order gives the order of considering each subsequent megabatch to introduce randomness. It was very hard to do this quickly within each megabatch
    print("filename_Table given below")
    print(filename_Table)

    local random_order = torch.randperm(number_of_batches)

    --set sigma to real variance (i.e. 0.2) if training the first layer
    if(layer_number == 1) then
        sigma = sigma_start
    end

    print("Starting repeat loop")
    while (repeatCounter < maxRepeat and convergenceFlag == 0) do
        repeatCounter = repeatCounter + 1
        print(" Progress repeatCounter: " .. repeatCounter .."/" ..maxRepeat)
        


        batch_complete_flag = 0
        mini_batch_number = 0
        --slightly redundant but clear intentions - definitely increase the batch being worked on, then mod to get batch_number in correct range
        --  so batch_number cycles through the megabatches available in the dataset folder provided.
        batch_number = batch_number + 1
        batch_number = ((batch_number - 1) % number_of_batches) + 1
        print("batch_number: " .. batch_number)
        if (batch_number == 1) then
            --we've finished going through each of the megabatches, re-randomise!
            random_order = torch.randperm(number_of_batches)
            if (repeatCounter == 1) then 
                batch_to_observe = filename_Table[random_order:data()[0]] --using batch_to_observe to check the progress of the hidden activations.
            end
        end

        print("Attempting to load megabatch with filename: " .. filename_Table[random_order:data()[batch_number-1]])
        --random_order:data() gets us the C pointers so arrays start at 0!! Also we use :data() to access the number itself
        local mega_batch_size, Nv, mega_batch = mega_batch_loader(filename_Table, random_order:data()[batch_number-1])
        Nh = Nv-Nw+1
        --print("Nv: " .. Nv .. ", Nw: " .. Nw .. ", Nh: " .. Nh)

        print("Loaded megabatch " .. random_order:data()[batch_number-1]..", and it has ".. mega_batch_size .." images in it" )
        --print("megabatch size is: ")
        --print(mega_batch:size())

        number_of_minibatches = math.ceil(mega_batch_size / mini_batch_size)
        print("Number of minibatches is: " .. number_of_minibatches)

        if(filename_Table[random_order:data()[batch_number-1]] == batch_to_observe) then 
            batch_to_observe_flag = 1
        end
        --set momentum to change after 5 epochs
        if(repeatCounter>5) then
            momentum = final_momentum
        end

        while(batch_complete_flag == 0 ) do
            mini_batch_number = mini_batch_number + 1
            --setting up the graph_number^th graph storage variables AND FILENAMES and FOLDER LOCATIONS to save into
            if(inner_while_repeat_counter % graph_inner_repeat_epoch_number == 0) then 
                graph_number = graph_number + 1
                print("New epoch!")
                filterWeights_heatmap = torch.Tensor(filterWeights:size()):zero() -- calculates the average movement for each filterweight
                filterWeights_directionality = torch.Tensor(filterWeights:size()):zero() -- calculates the average movement for each filterweight
                filterWeights_average_histogram = torch.Tensor(graph_inner_repeat_epoch_number,K,Vl):zero() -- saves what the average value for each filter is
                hiddenBias_graph = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
                hiddenBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
                visBias_graph = torch.Tensor(graph_inner_repeat_epoch_number, Vl):zero()
                visBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number, Vl):zero()
                reconstruction_error_graph = torch.Tensor(graph_inner_repeat_epoch_number):zero()
                hidden_activation_graph = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
            end
            --save filterWeights copies after every (large number of epochs) ??
            inner_while_repeat_counter = inner_while_repeat_counter + 1

            print("mini_batch_number: " .. mini_batch_number)
            --below calculates the actual size of the mini_batch - mega_batch_size gives the total number of images in the megabatch, and (m_b_num-1)*m_b_size should 
            --  theoretically be the number of images already processed, so the minimum should cap any index violations
            mini_batch_act_size = math.min(mini_batch_size, mega_batch_size - (mini_batch_number-1)*mini_batch_size)
            --print("mini_batch_act_size: " .. mini_batch_act_size)
            --so mini_batch should be a mini_batch_act_size x numPlanes x Nv x Nv minibatch of the megabatch of image data.
            local mini_batch = mega_batch:narrow(1,(mini_batch_number-1)*mini_batch_size+1,mini_batch_act_size)
            --print("mini_batch tensor size given below:")
            --print(mini_batch:size())
            --------------------------------------------------------------------------------------------------------------

            --MCMC UPDATES STEP - can expand this for CD-k, but for the moment just using CD-1
            --gibbsHiddenUpdate
            --CD Updater step.

	    --[[
            --Set sigma to the variance of the mini-batch if layer_number == 1 (i.e. the real RBM case), otherwise set it to 1
            --  (i.e. the binary case)
            if(layer_number ==1) then
                sigma = math.sqrt(mini_batch:var())
            else
                sigma = 1
            end
	    ]]--

            --Breals somewhere between here ...
            --print("Attempting CD Updates")i
            filterWeights = filterWeights/(sigma^2)
            local hiddenTensor = model:get(index):forward(mini_batch)
  --          print(model:get(index).weight[{1,{1,10}}])
	    filterWeights = filterWeights*(sigma^2)
            --hidden given visible
            --print(hiddenTensor:size())
            local hiddenTensor, origProbTensor =  gibbsHiddenUpdate(Nh,K,pooling_ratio_array[layer_number],mini_batch_act_size,hiddenTensor)

            --average hidden activation
            for i = 1,K do 
                hidden_activation_graph[{inner_while_repeat_counter,i}] = origProbTensor[{{},i }]:mean()
            end
            print("Average Hidden Activation: " .. origProbTensor:mean())

            if(batch_to_observe_flag == 1 and mini_batch_number == 1) then
                --print("Saving designated mini_batch hidden activations")
                batch_to_observe_counter = batch_to_observe_counter + 1
                hidden_activation_image_saver(origProbTensor, unique_Folder_Location, layer_number, batch_to_observe, batch_to_observe_counter, mini_batch_act_size,K)
                --save first mini_batch probability activations
            end
            --print("Check1")
            local mini_batch_generated = torch.Tensor(mini_batch:size())            
            
	    --Doing CD-1
	    for j = 1,1 do
            	if index == 1 then
            	    -- Rmini_batch_cloneeal Visible Layer 
            	    mini_batch_generated = gibbsRealVisibleUpdate(filterWeights,Nv,K,Vl,Nw,mini_batch_act_size,visBias,hiddenTensor)
            
            	else 
            	    -- Binary Visible Layer, since we always use the probabilties practically when sampling 
            	    --mini_batch_generated, prob_Visible_Tensor = gibbsBinaryVisibleUpdate(model,index,Nv,K,Vl,Nw,visBias,hiddenTensor)
            	    mini_batch_generated = gibbsBinaryVisibleUpdate(filterWeights,Nv,K,Vl,Nw,mini_batch_act_size,visBias,hiddenTensor)
            	end
	    	--Try changing to the binary visible version.
	    	--mini_batch_generated = gibbsBinaryVisibleUpdate(model,index,Nv,K,Vl,Nw,mini_batch_act_size,visBias,hiddenTensor)
                        
	    	--Adding variance for the real RBM case
            	filterWeights = filterWeights/(sigma^2)
            	hiddenTensor = model:get(index):forward(mini_batch_generated)
            	filterWeights = filterWeights*(sigma^2)
	   	-- print("Check2")
            	--Another MCMC updates step to for weights updates step.
            	hiddenTensor, prob_tensor_generated =  gibbsHiddenUpdate(Nh,K,pooling_ratio_array[layer_number],mini_batch_act_size,hiddenTensor)
            end
            --/MCMC UPDATES STEP.
            
            -- .... and here!! exception pop-ups aren't very helpful, go in manually, printing size of tensors etc.


            --Update weights and biases using the CD approximation---
            --learningRate will pop up here

            --gradFilterConv will be used in the parameter updates step, specifically to update the filters. Since
            --  the weight is tied to the hiddenTensor and NOT CLONED we should be able to update the convolution
            --  without having to constantly redeclare the convolution

            --Since each image in the mini_batch has to be convolved with its own hidden posterior we won't be able to take advantage of spatialConvolutionMM
            --  may as well stick to the nn.SpatialConvolution for simplicity.

            --print("Attempting Parameter Update convolutions")
            local gradFilterConv = nn.SpatialConvolution(1,K,Nh,Nh)
            gradFilterConv.bias = torch.Tensor(K):fill(0)
            --origSampleFilterWeightDifferential used as the base from which the filter weights CD gradient is 
            --    approximated; the summation convolution with the original training data sample
            
            --Below used as a cumulative now as we iterate over the set of images in the batch. The main thing that changed is that mini_batch and prob_tensor varieties
            --  are now 4D tensors instead of 3D tensors, the first dimension now used to index the image. By using the select shorthand tensor[i] we can reduce the 3D 
            --  tensors back to 3D tensors to fit easily into our original schema.

            --[[ Double Darn. Had the input and output planes swapped, meaning I couldn't be sure that the weight updates were even correct! Super frustrating.
            local origSampleFilterWeightDifferential = torch.Tensor(Vl,K,Nw,Nw):zero() 
            local newSampleFWDiff = torch.Tensor(Vl,K,Nw,Nw):zero()
            ]]--
            local origSampleFilterWeightDifferential = torch.Tensor(K,Vl,Nw,Nw):zero() 
            local newSampleFWDiff = torch.Tensor(K,Vl,Nw,Nw):zero()
	    
	    --print("Convolution Core Start")
            for i = 1, mini_batch_act_size do
                gradFilterConv.weight[{{},{1},{},{}}] = origProbTensor[i]
                for l = 1,Vl do
                    origSampleFilterWeightDifferential[{{},l ,{} ,{}}] = origSampleFilterWeightDifferential[{{},l ,{} ,{}}] + gradFilterConv:forward(mini_batch[{i,{l},{},{}}])
                end 
                gradFilterConv.weight[{{},{1},{},{}}] = prob_tensor_generated[i]
                for l = 1,Vl do 
                  newSampleFWDiff[{{},l ,{} ,{}}] = newSampleFWDiff[{{},l ,{} ,{}}] + gradFilterConv:forward(mini_batch_generated[{i,{l},{},{}}]) 
                end  
            end
	    --print("Convolution Core End")
            
            --------
            --filter weight updates, don't forget to divide by the number of images considered in the batch
--[[
            local filterWeights_addition = origSampleFilterWeightDifferential:add(-1,newSampleFWDiff)/(Nh*Nh*mini_batch_act_size)
            --reshape to go from nn.SpatialConvolution's weights to nn.SpatialConvolutionMM's weights
            -- local filterWeights_addition_MMResize = filterWeights_addition:reshape(filterWeights:contiguous, (K),(Vl*Nw*Nw))

            local sparsityPenaltyAddition = torch.Tensor(K):fill(sparsityPenalty) - origProbTensor:sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)
            local hiddenBias_addition = (origProbTensor:add(-1,prob_tensor_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)):add(sparsityRate,sparsityPenaltyAddition)
            local visBias_addition = (mini_batch-mini_batch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nv*Nv*mini_batch_act_size)
]]--
            if(filterWeights_addition == nil ) then
                --haven't been initialised yet, can't do momentum just yet
                filterWeights_addition = origSampleFilterWeightDifferential:add(-1,newSampleFWDiff)/(Nh*Nh*mini_batch_act_size)
                sparsityPenaltyAddition = torch.Tensor(K):fill(sparsityPenalty) - origProbTensor:sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)
                hiddenBias_addition = (origProbTensor:add(-1,prob_tensor_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)):add(sparsityRate,sparsityPenaltyAddition)
                visBias_addition = (mini_batch-mini_batch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nv*Nv*mini_batch_act_size)
                            else
                --updates have been initialised; can do momentum updates
                filterWeights_addition:mul(momentum):add(1,origSampleFilterWeightDifferential:add(-1,newSampleFWDiff)/(Nh*Nh*mini_batch_act_size))
                sparsityPenaltyAddition = torch.Tensor(K):fill(sparsityPenalty) - origProbTensor:sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)
                hiddenBias_addition:mul(momentum):add(1,origProbTensor:add(-1,prob_tensor_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nh*Nh*mini_batch_act_size)):add(sparsityRate,sparsityPenaltyAddition)
                visBias_addition:mul(momentum):add(1,((mini_batch-mini_batch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nv*Nv*mini_batch_act_size)))                
            end--updates have been initialised; can do momentum updates
        --[[
            if index == 1 then
                local visBias_addition = mini_batch:add(-1,mini_batch_generated):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nv*Nv*mini_batch_act_size)
            else
                local visBias_addition = mini_batch:add(-1,prob_Visible_Tensor):sum(1):sum(3):sum(4)[{1,{},1,1}]/(Nv*Nv*mini_batch_act_size)
            end
            ]]--

            --filterWeightDecay
            --filterWeights = filterWeights*0.99
            --convert from nn.spatconv's weight tensor to nn.spatConvMM's weight tensor
	    if(filterWeights:var() > 0.001) then
                filterWeights = filterWeights*0.99
            end
--	    filterWeights = filterWeights*0.9999

--[[
	    for i = 1,Vl do
		for j = 1,K do
		    filterWeights[{j ,{ (i-1)*Nw*Nw+1, i*Nw*Nw  }}] = filterWeights[{j ,{ (i-1)*Nw*Nw+1, i*Nw*Nw  }}] - filterWeights[{j ,{ (i-1)*Nw*Nw+1, i*Nw*Nw  }}]:mean()
		end
	    end
]]--
	    print(torch.abs(filterWeights_addition):mean()*learningRate[1] .." is average filterWeight_additon*learningRate[1]")
            --print("parameter updates step")
            filterWeights:add(learningRate[1], torch.reshape(filterWeights_addition:contiguous(),K,Vl*Nw*Nw) )
            hiddenBias:add(learningRate[2],hiddenBias_addition)
            visBias:add(learningRate[3],visBias_addition)
	    
            print("filterWeights min:" .. filterWeights:min())
	    print("filterWeights mean:" .. filterWeights:mean())
--[[
            --filterWeight control
            if (filterWeights:abs():max() > 1) then
                filterWeights = (filterWeights*1) /(filterWeights:abs():max())
            end
]]--
            --Calculate diagnostics
            mini_batch_recon_error = reconstruction_error(mini_batch,mini_batch_generated)
            print("reconstruction error for mini_batch " .. mini_batch_number .. " of mega_batch " .. random_order:data()[batch_number-1] .. ": " .. mini_batch_recon_error)
            mini_batch_cumulant_error = mini_batch_cumulant_error + mini_batch_recon_error	    
            --print("diagnostics updates")
            --adding to each of the diagnostics
            reconstruction_error_graph[inner_while_repeat_counter] = mini_batch_recon_error
            filterWeights_heatmap:add(learningRate[1],filterWeights_addition)
            filterWeights_directionality:add(1,torch.ge(filterWeights_addition,0):double())
            filterWeights_directionality:add(-1,torch.lt(filterWeights_addition,0):double()) --accumulates number of postive to negative moves
	    --[[
	    print("filterWeights:size()")
	    print(filterWeights:size())
	    print("filterWeights_mask:size()")
	    print(filterWeights_mask:size())
	    print("filterWeights_average_histogram[{inner_while_repeat_counter,{},{}}]:size()")
            print(filterWeights_average_histogram[{inner_while_repeat_counter,{},{}}]:size())
            ]]--
	    filterWeights_average_histogram[{inner_while_repeat_counter,{},{}}]:addmm(filterWeights, filterWeights_mask/(Nw*Nw))--some type of matrix multiplication to go here
            hiddenBias_graph[inner_while_repeat_counter] = hiddenBias
            hiddenBias_increments_histogram[inner_while_repeat_counter] = hiddenBias_addition*learningRate[2]
            visBias_graph[inner_while_repeat_counter] = visBias
            visBias_increments_histogram[inner_while_repeat_counter] = visBias_addition*learningRate[3]
            --if(torch.dist(visBias,vBCopy)+torch.dist(hiddenBias,hBCopy)+torch.dist(fWCopy,filterWeights) < convergenceThreshold ) then 
            --    convergenceFlag = 1
            --end

            --At this point we would have just finished collecting the statistics - now need to save them.
            if(inner_while_repeat_counter % graph_inner_repeat_epoch_number == 0) then
                print ("End of inner epoch " .. graph_number ..", attempting parameter_saver")
                parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number,graph_number)
		
                file = io.open(unique_Folder_Location.."/Diagnostics/Layer_".. layer_number .. "/Learning_Summary_File.txt","a")
                file:write("End of epoch " ..graph_number .." has reconstruction_error of:" ..mini_batch_recon_error.."\n")
                file:close()  --
                print("Attempting graph generation And Saving.")
                graph_and_histogram_saver(filterWeights_average_histogram,filterWeights_heatmap,filterWeights_directionality,hiddenBias_graph,hiddenBias_increments_histogram,visBias_graph,visBias_increments_histogram,reconstruction_error_graph,unique_Folder_Location, repeatCounter, layer_number,graph_number,Vl,K,Nw,hidden_activation_graph)
                --Saving and gnuplotting!
		inner_while_repeat_counter = 0
            end
            --calculate convergenceDistance and see whether to terminate process or keep going.           
            --if the below conditional is true then we should have iterated through the entire megabatch

            print()
            if(mini_batch_number == number_of_minibatches) then batch_complete_flag = 1 end
            collectgarbage("collect")
            --print("end inner while loop")
            midTime = os.time()
            print("Time Elapsed: " .. (midTime - startTime))
        end
        -- Diagnostic Monitors, histograms and such
        --  free Energy, reconstruction error
        print("Total error of mega_batch is: " .. mini_batch_cumulant_error)
        dynamic_repeat_counter = dynamic_repeat_counter + 1
        if(dynamic_repeat_counter == 0) then
            --first set up
            prev_recon_error = mini_batch_cumulant_error
        else
            if (prev_recon_error > mini_batch_cumulant_error) then 
                dynamic_learning_change_array[((dynamic_repeat_counter-1) % 10)+1] = -1
            else
                dynamic_learning_change_array[((dynamic_repeat_counter-1) % 10)+1] = 1
            end
            prev_recon_error = mini_batch_cumulant_error 
        end

        if(dynamic_repeat_counter > 9) then
            --Now change the learningRate
            local learn_change_sum = dynamic_learning_change_array:sum()
            print("recon error has consistently gone " ..learn_change_sum .. " times up over the last 10 mega_batches")
           --[[ 
            --positive learn_change_sum means recon error has goe overall up (in terms of how many times, not nec. abs amount)
            if learn_change_sum > 0 then
                learn_change_sum = learn_change_sum*3
            end
            learningRate = learningRate*(1 - learn_change_sum*0.01)
	    --It's not efficient but it'll work
	    for i = 1,3 do
                if(learningRate[i] < 0.1) then
                    learningRate[i] = 0.1
                end
                if(learningRate[i] > 0.1) then
                    learningRate[i] = 0.1
                end
            end
	    ]]--
        end
        print("learningRate is: " ..learningRate[1] .. ", " .. learningRate[2] .. ", " .. learningRate[3] )
	print("Average hiddenBias is: " ..hiddenBias:mean())
	print("Average FilterWeight is: " .. filterWeights:mean())
	print("Average VisBias is: " .. visBias:mean())
        mini_batch_cumulant_error = 0
        batch_to_observe_flag = 0

        --sigma_decay
        if(layer_number ==1) then
            if(sigma > sigma_stop) then
                sigma = sigma * 0.99
            end
        end

	print("end outer while loop")
    end
    
    --set repeatCounter to -1; indicates finishing
    --At this point the weights and hiddenBias and visBias should have been learned.
    repeatCounter = -1
    parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number)
    return filterWeights, hiddenBias, filename_Table
end
