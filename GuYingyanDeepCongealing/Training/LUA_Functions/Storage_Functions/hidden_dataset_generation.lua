require 'lfs'

function hidden_dataset_generator(model,layer_number, total_Layers,filename_table, unique_Folder_Location,pooling_Ratio_Array,hidden_Filter_Array,conv_Filter_Size_Array)
	--total_Layers is the total number of hidden layers trained - if layer_number == total_Layers we can use the whole model
	--  to generate the data-set, otherwise we go through the model one module at a time.
	--We are to save into directoryPath
	--Our data-set to generate FROM is given by filename_table
	local directoryPath = unique_Folder_Location .. "/Generated_Data/Layer_" .. layer_number
	local number_of_files = table.getn(filename_table)
	local genBatch
	for i = 1, number_of_files do 
		print("Batch " .. i.. "/" ..number_of_files)
		local mega_batch_size, Nv, mega_batch = mega_batch_loader(filename_table, i)
		genBatch = model:get((layer_number-1)*5 +1):forward(mega_batch)
		for j =2,5 do
			genBatch = model:get((layer_number-1)*5 + j):forward(genBatch) --whole model forward generation isn't going to work
		end
		--[[
		local randGen = torch.rand(genBatch:size())
		local toSave = torch.le(randGen,genBatch):double()
		print(genBatch:size())
		file = torch.DiskFile(directoryPath .."/generatedBatch"..i.. ".tns", 'w')
		file:writeObject(toSave)
		file:close()
		]]--
		print(genBatch:size())
		file = torch.DiskFile(directoryPath .."/generatedBatch"..i.. ".tns", 'w')
		file:writeObject(genBatch)
		file:close()
		genBatch = nil
	end	




end
