require 'lfs'

function create_Model_Folders(current_dir,date_table,num_Hidden_Layers,hidden_Filter_Array,pooling_Ratio_Array,conv_Filter_Size_Array,current_time,sparsity_Penalty_Array)
	--unique_folder_location relative to where trainingMain.lua is
	local unique_folder_location = "/Models/" .. date_table["year"] .. "-" .. date_table["month"] .. "-" .. date_table["day"] .. "_" .. date_table["hour"] .. "-" .. date_table["min"] .. "-".. date_table["sec"]
	unique_folder_location = unique_folder_location .. "_L" .. num_Hidden_Layers

	for i = 1, num_Hidden_Layers do 
		unique_folder_location = unique_folder_location .. "_K" ..i .."-" ..hidden_Filter_Array[i]
	end

	--Make Base Directory
	local full_path = current_dir .. unique_folder_location
	lfs.mkdir(full_path)

	--Make subDirectories
	lfs.mkdir(full_path.."/Parameters")
	lfs.mkdir(full_path.."/Generated_Data")
	lfs.mkdir(full_path.."/Diagnostics")

	for i = 1,num_Hidden_Layers do
		lfs.mkdir(full_path.."/Parameters/Layer_" ..i)
		lfs.mkdir(full_path.."/Parameters/Layer_" ..i.."/Initial")
		lfs.mkdir(full_path.."/Parameters/Layer_" ..i.."/During")
		lfs.mkdir(full_path.."/Parameters/Layer_" ..i.."/Final")

		lfs.mkdir(full_path.."/Generated_Data/Layer_"..i)
		lfs.mkdir(full_path.."/Generated_Data/Layer_" ..i.."/Raw_Probabilities")
		lfs.mkdir(full_path.."/Generated_Data/Layer_" ..i.."/Samples")

		lfs.mkdir(full_path.."/Diagnostics/Layer_"..i)
		lfs.mkdir(full_path.."/Diagnostics/Layer_"..i.."/Graphs")
		lfs.mkdir(full_path.."/Diagnostics/Layer_"..i.."/Histograms")
		lfs.mkdir(full_path.."/Diagnostics/Layer_"..i.."/Hidden_Activations")

	end
	--print(full_path)
	--print(current_time)
	--Write Initial Summary file
	file = io.open(full_path .. "/Summary_File.txt","w")
	local writeString = "Model Initialised on " .. os.date("%c",current_time) .. "\n"
        file:write(writeString)
	writeString = "Model's unique_folder_location is: " .. full_path .. "\n"
	file:write(writeString)
	writeString = "Model has " .. num_Hidden_Layers .. " Hidden Layer(s)" .. "\n \n"
	file:write(writeString)
	for i = 1,num_Hidden_Layers do
		writeString = "Hidden Layer "..i.." has a Pooling Ratio of " .. pooling_Ratio_Array[i] .. "\n"
		file:write(writeString)
		writeString = "Hidden Layer "..i.." has a Convolution Filter Size of " ..conv_Filter_Size_Array[i].."\n"
		file:write(writeString)
		writeString = "Hidden Layer "..i.." has " ..hidden_Filter_Array[i].." filters\n \n"
		file:write(writeString)
		writeString = "Hidden Layer "..i.." has target sparsity " ..sparsity_Penalty_Array[i].." filters\n \n"
		file:write(writeString)
	end
	file:close()
	return full_path
end
