function parameter_saver(filterWeights,hiddenBias,visBias,unique_Folder_Location, repeatCounter, layer_number,graph_number)
	local save_path
	local append
	if repeatCounter == 0 then
		--Saving the initial parameters
		 save_path = unique_Folder_Location .."/Parameters/Layer_" .. layer_number .."/Initial/"
		 append = "_Start_"
	elseif repeatCounter > 0 then
		--Saving the parameters during training
		 save_path = unique_Folder_Location .."/Parameters/Layer_" .. layer_number .."/During/"
		 append = "_" ..graph_number .. ""
	else
		--Saving the final parameters
		 save_path = unique_Folder_Location .."/Parameters/Layer_" .. layer_number .."/Final/"
		 append = "_Final"
	end

	file = torch.DiskFile(save_path .. "filterWeights".. append .. ".tns", 'w')
	file:writeObject(filterWeights)
	file:close()
	file = torch.DiskFile(save_path .. "hiddenBias".. append .. ".tns", 'w')
	file:writeObject(hiddenBias)
	file:close()
	file = torch.DiskFile(save_path .. "visBias".. append .. ".tns", 'w')
	file:writeObject(visBias)
	file:close()
end
