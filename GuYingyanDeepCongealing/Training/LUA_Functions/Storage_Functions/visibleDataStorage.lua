require 'image'
require 'lfs'
require 'torch'
signal = require 'signal'
print("Starting Search")
-- directoryPath gives the directory of the LFW
--  runs relative to where visibleDataStorage is
filenameTable = {}
counter = 0
directoryPath = "../../../DataSet/lfw"
for foldername in lfs.dir(directoryPath) do
    --LFW is made entirely of folders. The next loop should access all of the images
    for filename in lfs.dir(directoryPath .. "/" ..foldername) do
        if filename:match("%.jpg$") then --Will match any filenames that have ".jpg" at the end of the string
            counter = counter + 1
            filenameTable[counter] = directoryPath .. "/" .. foldername .. "/" ..filename
            --print(filename)
        end
    end
end

print(counter .. " jpg files found")
numberOfJPGs = counter
batch_size = 50
numBatches = math.ceil(numberOfJPGs/batch_size)

-- Setting up FFT filters, assuming image size to be 512
N = 250
linspace = torch.linspace(-N/2,N/2-1, N)
fourier_x = torch.Tensor(N,N)
fourier_y = torch.Tensor(N,N)
for i = 1,N do
    fourier_x[{i,{}}] = linspace
    fourier_y[{{},i}] = linspace
end
rho = torch.sqrt((torch.pow(fourier_x,2)+torch.pow(fourier_y,2))):t()

rhoAlphaPow = rho:clone()
alpha = 2.5 --alpha parameter
rhoAlphaPow = torch.pow(rhoAlphaPow,(alpha/2))

fourier_0 = 0.4*N
filter = torch.Tensor(N,N)
filter =torch.cmul(rhoAlphaPow,torch.exp(- torch.pow((rho/fourier_0),4) ) )
filterShift = torch.Tensor(N,N,2)
filterShift[{{1,N/2},{1,N/2},1}] = filter[{{N/2+1,N},{N/2+1,N}}]
filterShift[{{1,N/2},{N/2 + 1,N},1}] = filter[{{N/2+1,N},{1,N/2}}]
filterShift[{{N/2+1,N},{1,N/2},1}] = filter[{{1,N/2},{N/2+1,N}}]
filterShift[{{N/2+1,N},{N/2+1,N},1}] = filter[{{1,N/2},{1,N/2}}]
filterShift[{{},{},2}] = filterShift[{{},{},1}]
sqrtOf01 = torch.sqrt(0.1)
--
-- Just saving one batch
for j = 1,1 do
	print("Working on Batch " .. j)
	dataSet = torch.Tensor(batch_size,1,N,N) --Assumes all images will be 250x250 and converted into Y only

	for i = 1+(j-1)*batch_size,math.min(j*batch_size,numberOfJPGs) do
	    print("Processing image "..i)
--[[	    floatImage = image.scale(image.load(filenameTable[i],1) ,100,100) 
	    dataSet[{(i-1)%500+1,1,{},{}}] = floatImage
]]--	
	    X = image.scale(image.load(filenameTable[i],1) ,N,N) 
	    -- Whitening and PreProcessing
	    mX2 = X - X:mean()
		mX2 = mX2/mX2:std()

		--[[ Whitening
		FFTX = signal.fft2(mX2)
		wX2PreNorm = signal.ifft2(torch.cmul(FFTX,filterShift) )
		wX2 = wX2PreNorm[{{},{},1}]/wX2PreNorm[{{},{},1}]:std()
		wX2 = wX2 - wX2:mean()		--Should be zero mean and variance 1 at this point
		wX2:mul(sqrtOf01)		--for some reason Lee multiplies the data by sqrt(0.1) - even he has a question mark in his code
		dataSet[{(i-1)%500+1,1,{},{}}] = wX2
		]]--

		--Just normal variance division and mX2 stuff.
		mX2:mul(sqrtOf01)
	    dataSet[{(i-1)%500+1,1,{},{}}] = mX2
	    --Saves as a imageNumxVisPlanexNhxNw
	end
	file = torch.DiskFile("../../../DataSet/Training/" .."visibleBatch"..j.. ".tns", 'w')
	file:writeObject(dataSet)
	file:close()
	collectgarbage("collect")
end
