require 'gnuplot'
function graph_and_histogram_saver(filterWeights_average_histogram,filterWeights_heatmap,filterWeights_directionality,hiddenBias_histogram,hiddenBias_increments_histogram,visBias_graph,visBias_increments_histogram,reconstruction_error_graph,unique_Folder_Location, repeatCounter, layer_number,graph_number,Vl,K,Nw,hidden_activation_graph)
	local save_path = unique_Folder_Location .."/Diagnostics/Layer_" .. layer_number 
	local filename

	--Save filterWeight pngs
--[[
	for i = 1,Vl do
		for j = 1,K do 
			--plotting ith visible layer to jth hidden layer weight matrix
			--filterWeights_heatmap a K by Vl*Nw*Nw tensor
			local toPlot = filterWeights_heatmap[{j,{ (i-1)*Nw*Nw + 1, i*Nw*Nw  }}]:clone()
			filename = save_path .. "/" .. graph_number .. "_Epoch_filterWeights_heatmap_VisLay-" .. i .."_HidLay-"..j  ..".png"
			gnuplot.pngfigure(filename)
			gnuplot.plot(toPlot, 'color')
			gnuplot.title("Filter Weights Heatmap No. " .. graph_number .."; Visible Layer ".. i ..", Hidden Layer " ..j)
			gnuplot.plotflush()
		end
	end

	for i = 1,Vl do
		for j = 1,K do 
			--plotting ith visible layer to jth hidden layer weight matrix
			--filterWeights_heatmap a K by Vl*Nw*Nw tensor
			local toPlot = filterWeights_directionality[{j,{ (i-1)*Nw*Nw + 1, i*Nw*Nw  }}]:clone()
			filename = save_path .. "/" .. graph_number .. "_Epoch_filterWeights_directionality_VisLay-" .. i .."_HidLay-"..j  ..".png"
			gnuplot.pngfigure(filename)
			gnuplot.plot(toPlot, 'color')
			gnuplot.title("Filter Weights Directionality Map No. " .. graph_number .."; Visible Layer ".. i ..", Hidden Layer " ..j)
			gnuplot.plotflush()
		end
	end
]]--
	for i = 1,1 do 
		--Plotting the average values of the filterWeight kernels
		local testArray = {}
		for j = 1,K do 
		    testArray[j] ={'Hid. Lay. '.. j,filterWeights_average_histogram[{{},j,i  }],'~'}
		end
		filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_filterWeight_Averages_VisLay-" .. i ..".png" 
		gnuplot.pngfigure(filename)
	        gnuplot.plot(testArray)
		gnuplot.title("Filter Weight Average of Kernels graph No. ".. graph_number .."; Visible Layer ".. i )
		gnuplot.ylabel('Average')
		gnuplot.xlabel('Iteration')
		gnuplot.plotflush()
	end
--[[
	                        --plotting ith visible layer to jth hidden layer weight matrix
                        --filterWeights_heatmap a K by Vl*Nw*Nw tensor
        local toPlot = filterWeights_heatmap[{1,{ 1, Nw*Nw  }}]:clone()
        filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_filterWeights_heatmap_VisLay-1_HidLay-1.png"
        gnuplot.pngfigure(filename)
        gnuplot.imagesc(toPlot:resize(Nw,Nw), 'color')
        gnuplot.title("Filter Weights Heatmap No. " .. graph_number .."; Visible Layer 1, Hidden Layer 1")
        gnuplot.plotflush()

        local toPlot = filterWeights_directionality[{1,{ 1, Nw*Nw  }}]:clone()
        filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_filterWeights_directionality_VisLay-1_HidLay-1.png"
        gnuplot.pngfigure(filename)
        gnuplot.imagesc(toPlot:resize(Nw,Nw), 'color')
        gnuplot.title("Filter Weights Heatmap No. " .. graph_number .."; Visible Layer , Hidden Layer 1" )
        gnuplot.plotflush()
]]--
	--hiddenBias Values
	local testArray = {}
	for j = 1,K do 
	    testArray[j] ={'Hid. Lay. '.. j,hiddenBias_histogram[{{},j  }],'~'}
	end
	filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_hiddenBias.png" 
	gnuplot.pngfigure(filename)
        gnuplot.plot(testArray)
	gnuplot.title("Hidden Bias graph No. ".. graph_number )
	gnuplot.xlabel('Iteration')
	gnuplot.plotflush()

	--visBias Values
	local testArray = {}
	for j = 1,Vl do 
	    testArray[j] ={'Vis. Lay. '.. j,visBias_graph[{{},j  }],'~'}
	end
	filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_visBias.png" 
	gnuplot.pngfigure(filename)
        gnuplot.plot(testArray)
	gnuplot.title("Visible Bias graph No. ".. graph_number )
	gnuplot.xlabel('Iteration')
	gnuplot.plotflush()

	--hidden_activation Values
	local testArray = {}
	for j = 1,Vl do 
	    testArray[j] ={'Hid. Lay. '.. j,hidden_activation_graph[{{},j  }],'~'}
	end
	filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_Hidden_Activations.png" 
	gnuplot.pngfigure(filename)
        gnuplot.plot(testArray)
	gnuplot.title("Hidden Activation graph No. ".. graph_number )
	gnuplot.xlabel('Iteration')
	gnuplot.plotflush()

    --local hiddenBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number,K):zero()
	local toHist = torch.Tensor(100,K)
	local xscale = torch.Tensor(K,100)
	local testArray = {}
	for i = 1,K do
		toHist[{{},i}] = torch.histc(hiddenBias_increments_histogram[{{}, i}])
		local min = hiddenBias_increments_histogram[{{}, i}]:min()
		local max = hiddenBias_increments_histogram[{{}, i}]:max()
		xscale[i] = torch.linspace(min, max)
		testArray[i] ={'Hid. Lay. '.. i,xscale[i],toHist[{{},i  }],'|'}
	end
	filename = save_path .. "/Histograms/" .. graph_number .. "_Epoch_hidBias_Increments_Histogram.png" 
	gnuplot.pngfigure(filename)
        gnuplot.plot(testArray)
	gnuplot.title("Hidden Bias Increment graph No. ".. graph_number )
	gnuplot.xlabel('Increment')
	gnuplot.plotflush()


    --local visBias_increments_histogram = torch.Tensor(graph_inner_repeat_epoch_number, Vl):zero()
	local toHist = torch.Tensor(100,K)
	local xscale = torch.Tensor(K,100)
	local testArray = {}
	for i = 1,Vl do
		toHist[{{},i}] = torch.histc(visBias_increments_histogram[{{}, i}])
		local min = visBias_increments_histogram[{{}, i}]:min()
		local max = visBias_increments_histogram[{{}, i}]:max()
		xscale[i] = torch.linspace(min, max)
		testArray[i] ={'Vis. Lay. '.. i,xscale[i],toHist[{{},i  }],'|'}
	end
	filename = save_path .. "/Histograms/" .. graph_number .. "_Epoch_visBias_Increments_Histogram.png" 
	gnuplot.pngfigure(filename)
    gnuplot.plot(testArray)
	gnuplot.title("Visible Bias Increment graph No. ".. graph_number )
	gnuplot.xlabel('Increment')
	gnuplot.plotflush()


    --local reconstruction_error_graph = torch.Tensor(graph_inner_repeat_epoch_number):zero()
    filename = save_path .. "/Graphs/" .. graph_number .. "_Epoch_Reconstruction_Error.png" 
	gnuplot.pngfigure(filename)
    gnuplot.plot({reconstruction_error_graph,'~'})
	gnuplot.title("Reconstruction Error graph No. ".. graph_number )
	gnuplot.xlabel('Iteration')
	gnuplot.plotflush()

	gnuplot.closeall()
end
