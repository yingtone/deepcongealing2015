function mega_batch_loader(filename_table, batch_number)

	local testReadFile = torch.DiskFile(filename_table[batch_number], 'r')
	local testRead = testReadFile:readObject()

	--testRead should be a numImages x numVisPlanes x Nv x Nv tensor

	local number_of_images = testRead:size()[1]
	local Nv = testRead:size()[3]

	return number_of_images, Nv, testRead
end