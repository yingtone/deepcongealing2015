function dataset_filename_retriever(model,layer_number,unique_Folder_Location)
	local filenameTable = {}
	local counter = 0

	--layer_number is the layer that we wish to train - so layer_number == 1 corresponds to training the first hidden layer
	--  the below conditional sets where the folder is to look for the .tns files 
	if layer_number == 1 then
		--visible layer is stored in ../../../DataSet/training
		--directoryPath = unique_Folder_Location .. "/../../../DataSet/Training"
		directoryPath = unique_Folder_Location .. "/../../../DataSet/TrainingAlphaMod"
	else
		--
		directoryPath = unique_Folder_Location .. "/Generated_Data/Layer_" .. (layer_number - 1)
	end	

    for filename in lfs.dir(directoryPath) do
        if filename:match("%.tns$") then --Will match any filenames that have ".jpg" at the end of the string
            counter = counter + 1
            filenameTable[counter] = directoryPath  .. "/" ..filename
        end
    end

    --counter at this stage returns how many tensors have been saved to be trained with.
    return counter, filenameTable
end
