require 'image'
require 'lfs'

function hidden_activation_image_saver(orig_prob_tensor, unique_folder_location, layer_number, batch_to_observe, batch_to_observe_counter, mini_batch_act_size,K)
	if (batch_to_observe_counter == 1) then
		--first time we've saved the summary file
		file = io.open(unique_folder_location.."/Diagnostics/Layer_".. layer_number .. "/Hidden_Activations/Hidden_Activations_Summary_File.txt","w")
		file:write("Working with batch with filename: " .. batch_to_observe .. "  \n")
		file:close()  --
	end
--[[
	local filename
	for i = 1,mini_batch_act_size do 
		for j = 1, K do 
			filename = unique_folder_location.."/Diagnostics/Layer_".. layer_number .. "/Hidden_Activations/batch_epoch_" 
			filename = filename .. (batch_to_observe_counter-1) .. "-image_" .. i .. "-filter_" ..j .. ".png"
			image.save(filename, orig_prob_tensor[{i,{j},{},{} }])
		end
	end
	filename = unique_folder_location.."/Diagnostics/Layer_".. layer_number .. "/Hidden_Activations/"..(batch_to_observe_counter-1) .."_Sample_Tensor.txt"
	file = torch.DiskFile(filename, 'w')
	file:writeObject(orig_prob_tensor[{1,1,{},{}}])
	file:close()
]]--
	local average_activation = orig_prob_tensor:sum(1):sum(2):sum(3):sum(4)[{1,1,1,1}]/orig_prob_tensor:nElement()
	file = io.open(unique_folder_location.."/Diagnostics/Layer_".. layer_number .. "/Hidden_Activations/Hidden_Activations_Summary_File.txt","a")
	file:write("Average Activation after Epoch " .. (batch_to_observe_counter-1) .. " is: "  .. average_activation .."  \n")
	file:close()  --

end
