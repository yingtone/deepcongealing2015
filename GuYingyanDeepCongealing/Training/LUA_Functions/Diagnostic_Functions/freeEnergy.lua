
--a Function that should return the free energy of a dataset through which we will infer roughly
--  what the likelihood is doing.

function freeEnergy(model,index,dataSet,visBiasFE,Nh,K,Nw)
    --Assume that the dataSet is NxVlxNvxNv, i.e. the N visible images with all input planes.
    visClone = dataSet:clone()
    visClone2 = dataSet:clone()
    cumulativeFreeEnergy = 0
    
    N = dataSet:size()[1]
        
    for i = 1,N do
        --In each loop iteration we add up the ith image's contribution to the FreeEnergy
        modelOutput = model:get(index):forward(visClone[i])
        dummyTensor, posteriorTensor = gibbsHiddenUpdate(Nh,K,Nw,modelOutput)
        
        --dummyTensor a dummy variable at the moment, but if we do decide to use the sample hidden activations
        --  then dummyTensor has these available. posteriorTensor we will use in calculating the energy
        
        --At this point, modelOutput should be a KxNhxNh tensor, as should posteriorTensor
        --  so just need to element-wise multiply the tensors, then sum along the first, second and third 
        --  dimension.
        
        cumulativeFreeEnergy = cumulativeFreeEnergy - modelOutput:cmul(posteriorTensor):sum(1):sum(2):sum(3):storage()[1]
                        
        --Now need to account for the energy contributions of the visible parts alone
        cumulativeFreeEnergy = cumulativeFreeEnergy + visClone[i]:pow(2):sum(1):sum(2):sum(3):storage()[1]/2 - visClone2[i]:sum(2):sum(3):cmul(visBiasFE):sum(1):storage()[1]
    end

    return cumulativeFreeEnergy
end

--a Function that should return the free energy of a dataset through which we will infer roughly
--  what the likelihood is doing.

function freeEnergyAndAverageActivations(model,index,dataSet,visBiasFE,Nh,K,Nw)
    --Assume that the dataSet is NxVlxNvxNv, i.e. the N visible images with all input planes.
    visClone = dataSet:clone()
    visClone2 = dataSet:clone()
    cumulativeFreeEnergy = 0
    aveActiv = 0
    N = dataSet:size()[1]
        
    for i = 1,N do
        --In each loop iteration we add up the ith image's contribution to the FreeEnergy
        modelOutput = model:get(index):forward(visClone[i])
        dummyTensor, posteriorTensor = gibbsHiddenUpdate(Nh,K,Nw,modelOutput)
        
        --dummyTensor a dummy variable at the moment, but if we do decide to use the sample hidden activations
        --  then dummyTensor has these available. posteriorTensor we will use in calculating the energy
        
        --At this point, modelOutput should be a KxNhxNh tensor, as should posteriorTensor
        --  so just need to element-wise multiply the tensors, then sum along the first, second and third 
        --  dimension.
        
        cumulativeFreeEnergy = cumulativeFreeEnergy - modelOutput:cmul(posteriorTensor):sum(1):sum(2):sum(3):storage()[1]
                        
        --Now need to account for the energy contributions of the visible parts alone
        cumulativeFreeEnergy = cumulativeFreeEnergy + visClone[i]:pow(2):sum(1):sum(2):sum(3):storage()[1]/2 - visClone2[i]:sum(2):sum(3):cmul(visBiasFE):sum(1):storage()[1]
        aveActiv = aveActiv + posteriorTensor:sum(1):sum(2):sum(3):storage()[1]/(K*Nh*Nh*N)
    end

    return cumulativeFreeEnergy, aveActiv
end
