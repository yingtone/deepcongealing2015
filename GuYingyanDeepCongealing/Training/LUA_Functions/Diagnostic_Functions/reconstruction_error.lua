function reconstruction_error(mini_batch, mini_batch_generated)


	return torch.dist(mini_batch, mini_batch_generated)/mini_batch:size()[1]
end
