require 'torch'
require 'lfs'
require 'image'

--adding LUA_Functins folders to the package path
local current_dir= lfs.currentdir()
package.path = package.path ..";" .. current_dir .."/LUA_Functions/CDBN_Functions/?.lua"
package.path = package.path ..";" .. current_dir .."/LUA_Functions/Diagnostic_Functions/?.lua"
package.path = package.path ..";" .. current_dir .."/LUA_Functions/Storage_Functions/?.lua"
require("CDBNModelBuilderFunction")
require("model_Folders_Creator")
require("CDBNLayerTrainer")
require("CDBNLayerTrainer2")
require("hidden_dataset_generation")

--This is the version that restarts training. A custom function that requires tweaking, the main parameters to change are which layers
--  are already trained (we will assume that this is greater than 1, otherwise we would run trainingMain.lua), and to set unique_folder_
-- location to the correct Model folder.

--Initialising the Model
local current_time = os.time()
local date_table = os.date("*t",current_time)
local num_Hidden_Layers = 3
local pooling_Ratio_Array = {2,2,2}
local hidden_Filter_Array = {24,48,48}
local conv_Filter_Size_Array = {10,10,10}
local sigma_array = {0.1,1,1}
local model = CDBNModelBuilder(num_Hidden_Layers,pooling_Ratio_Array,hidden_Filter_Array,conv_Filter_Size_Array)

local Vl_Array = {1}
for i = 2,num_Hidden_Layers do
        Vl_Array[i] = hidden_Filter_Array[i-1]
end

local sparsity_Penalty_Array = {0.003,0.005,0.005}

--local learning_rate_array = {0.00005,0.0001,0.0001}
local learning_rate_array = torch.Tensor(3)
learning_rate_array[1] = 0.01
learning_rate_array[2] = 0.01
learning_rate_array[3] = 0.01
local sparsity_rate_array ={5,2,2}
--Creating Folder Structure
--  unique_Folder_Location is the full directory path to the .../Models/XXXX-XX... folder location of the initialised model
--local unique_Folder_Location = create_Model_Folders(current_dir,date_table,num_Hidden_Layers,hidden_Filter_Array,pooling_Ratio_Array,conv_Filter_Size_Array,current_time,sparsity_Penalty_Array)

local layers_to_train_start = 2 --i.e. if this was 2 we would start training the 2nd hidden layer upwards. We assume that the data is already
--  generated
local unique_Folder_Location = current_dir .. "/Models/2015-8-25_19-41-19_L3_K1-24_K2-48_K3-48"

-- preload the parameters ito the created model
for i = 1,layers_to_train_start-1 do
        local parameter_path = unique_Folder_Location .."/Parameters/Layer_" .. i .. "/Final/"
        local testReadFile = torch.DiskFile(parameter_path .. "/filterWeights_Final.tns", 'r')
        local fW = testReadFile:readObject()
        --sigma division only needed in one corrective case
        --model:get((i-1)*5 + 1).weight = fW:clone()
        model:get((i-1)*5 + 1).weight = fW:clone()/(sigma_array[i]^2)
        local testReadFile = torch.DiskFile(parameter_path .. "/hiddenBias_Final.tns", 'r')
        local hB = testReadFile:readObject()
        --model:get((i-1)*5 + 1).bias = hB:clone()
        model:get((i-1)*5 + 1).bias = hB:clone()/(sigma_array[i]^2)
end

local filename_Table = {unique_Folder_Location .. "/../../../DataSet/Training/visibleBatch1.tns"}
--regenning first layer
hidden_dataset_generator(model,1, num_Hidden_Layers,filename_Table, unique_Folder_Location,pooling_Ratio_Array,hidden_Filter_Array,conv_Filter_Size_Array)

local max_repeat_array = {500,500,500}
--Training
for i = layers_to_train_start,num_Hidden_Layers do
        local index = (i-1)*5 + 1
        print("Attempting CDBNLayerTrainer2 Call to train Layer " .. i)
        filterWeights, hiddenBias, filename_Table =CDBNLayerTrainer2(model,i,unique_Folder_Location,learning_rate_array,sparsity_Penalty_Array[i],sparsity_rate_array[i],hidden_Filter_Array[i],Vl_Array[i],max_repeat_array[i],pooling_Ratio_Array,sigma_array[i])
        model:get((i-1)*5 + 1).weight = filterWeights:clone()/(sigma_array[i]^2)
        model:get((i-1)*5 + 1).bias = hiddenBias:clone()/(sigma_array[i]^2)
        --model gets the effective filter parameters
        print("Attempting hidden dataset generation of Layer" .. i)
        if(i ~= num_Hidden_Layers) then
		hidden_dataset_generator(model,i, num_Hidden_Layers,filename_Table, unique_Folder_Location,pooling_Ratio_Array,hidden_Filter_Array,conv_Filter_Size_Array)
	end
end

--Saving Final Model; find a way to make this use less memory
for i = 1,(num_Hidden_Layers*5) do
	model:get(i).output = nil
        model:get(i).finput = nil
        model:get(i).temp = nil
end
model.output = nil

file = torch.DiskFile(unique_Folder_Location .."/Final_Model.mdl","w")
file:writeObject(model)
file:close()

