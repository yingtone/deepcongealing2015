
require('unsup')

--This function should initialise the parameters via the K-means then GMM process as specified 

function parameterInitialiser(trainingSet,K,Vl,Nv,N,Nw)
    --trainingSet should be a NxLxNvxNv large tensor
    --  To initialise the K-Means process, need to resize as an MxNDim matrix; NDim = L, M = N*Nv*Nv
    
    --  How to resize efficiently? Consider a visible input plane at a time, then take advantage of the
    --    reshape along the relevant index of the visible input index dimension.
    inputTensor = torch.Tensor(N*Nv*Nv,Vl)
    for i = 1,Vl do
        inputTensor[{{},i}] = torch.reshape(trainingSet:contiguous():select(2,i),N*Nv*Nv)
    end
    
    --KMeans stuff goes here
    --At this point should have a K+1 by L tensor, and a tensor that counts membership to each centroid
    --  centroids
    --  countsPerCentroid
    
    --Naive Initialisation, evenly spreading out the centroids amongst the min and max of the ith vis-Plane
    centroids = torch.Tensor(K+1,Vl)
    for i = 1,Vl do
        centroids[{{},i}] = torch.linspace(trainingSet:select(2,i):min(),trainingSet:select(2,i):max(),K+1)
    end
    
    --Estimate sigmaSquared here
    --  Assume that sigma = 1 as before
    
    --GMM goes here
    
    --RBM parameter initialisation goes here
    --  i.e. here we convert to the RBM parameters as given in the paper
    
    vB = centroids[1]
    initFW = torch.Tensor(K,Vl)
    initBias = torch.Tensor(K)
    for i = 2,K+1 do
        initFW[i-1]= centroids[i] - vB
        initBias[i-1] = torch.Tensor(1):fill(-0.5*torch.norm(initFW[i-1])) - torch.cmul(initFW[i-1],vB):sum(1)
    end
    
    --CRBM parameter initialisation goes here
    --  So initBias and vB should be as previously calculated. 
    
    FW = torch.Tensor(K,Vl,Nw,Nw)
    for i = 1,K do
        for j = 1,Vl do
            FW[{i,j,{},{}}]:fill(initFW[{i,j}]/2)
            --Insert random perturbation
            FW[{i,j,{},{}}] = FW[{i,j,{},{}}] + torch.randn(Nw,Nw):cmul(torch.rand(Nw,Nw))/K
        end
    end
    return FW, initBias, vB
end
