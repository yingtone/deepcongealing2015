
-- require 'nn'
-- require 'image' --necessary?

-- inputs: the Model itself, the index of the layer in question.
--   How to handle the data-set? We don't want too much memory copy, so I propose that in the training of the 
--   model we pass handles to a clone of the training data-set of that particular layer. The function should
--   then change the contents of the storage that that training data-set refers to.

-- visible given hidden, two functions, realVisible and binaryVisible. The number of visibleLayers shouldn't 
--   change the underlying code.

--DEBUG - giving huge discrepancies of numbers

function gibbsRealVisibleUpdate(model,index,Nv,K,Vl,Nw,visBias,hiddenNodeTensor)
  -- cap proposals? Currently it can propose visible values outside 0 and 1 which doesn't agree with the orig. image
  visConv = nn.SpatialConvolution(K,Vl,Nw,Nw,1,1,Nw-1,Nw-1) 
  --  Need to Import filter parameters here. Need to swap first and second dimensions (input to output planes)
  visConv.weight = model:get(index).weight
  visConv.weight = image.flip(visConv.weight,3) --Re-ordered flip to prevent contiguous error flip
  visConv.weight = image.flip(visConv.weight,4)
  visConv.weight:transpose(1,2) --swaps first and second dimension i.e. input and 
    --output planes as necessary
  visConv.bias = visBias
  -- meanTensor = visConv:forward(hiddenNodeTensor)
  --visibleNodeTensor = meanTensor + torch.randn(Vl,Nv,Nv)
  
  --Implementing different return - capping visible value returns.
  return visConv:forward(hiddenNodeTensor) + torch.randn(Vl,Nv,Nv)
  --returnTensor = visConv:forward(hiddenNodeTensor) + torch.randn(Vl,Nv,Nv)
  --returnTensor[torch.le(returnTensor,0)] = 0
  --returnTensor[torch.ge(returnTensor,1)] = 1
  --return returnTensor
end

function gibbsBinaryVisibleUpdate(model,index,Nv,K,Vl,Nw,visBias,hiddenNodeTensor)
  visConv = nn.SpatialConvolution(K,Vl,Nw,Nw,1,1,Nw-1,Nw-1) 
  --  Need to Import filter parameters here. Need to swap first and second dimensions (input to output planes)
  visConv.weight = model:get(index).weight
  visConv.weight = image.flip(visConv.weight,3) --Re-ordered flip to prevent contiguous error flip
  visConv.weight = image.flip(visConv.weight,4)
  visConv.weight:transpose(1,2) --swaps first and second dimension i.e. input and 
    --output planes as necessary
  visConv.bias = visBias
  --meanTensor = visConv:forward(hiddenNodeTensor)
  probVTensor = nn.Sigmoid():forward(visConv:forward(hiddenNodeTensor)) --Gets the probability of visible node being active
  unifProbs = torch.rand(Vl,Nv,Nv) --Needs to be a numVisPlanesxNvxNv uniform probability tensor
  --visibleNodeTensor = torch.le(unifProbs,probTensor):double()
  return torch.le(unifProbs,probVTensor):double()
end

function gibbsHiddenUpdate(Nh,K,Nw,hiddenNodeTensor)
  numeTensor = hiddenNodeTensor:exp() --assuming that hiddenNodeTensor has already been calculated.
    --Want to keep this function as fast as possible. If needed though can pull the model into this function
  sumDenomConv = nn.SpatialConvolution(1,1,Nw,Nw,Nw,Nw)
  sumDenomConv.weight = torch.ones(1,1,Nw,Nw) --After each forward we have the inputs for the i'th filter as per
--    the for loop
  sumDenomConv.bias = torch.Tensor(1):fill(1)
  sumNumeTensor = nn.SpatialZeroPadding(0,Nw-Nh%Nw,0,Nw-Nh%Nw):forward(numeTensor) -- pads so that we get
    --proper addition of all possible hidden node values; getting around SpatialConvolution quirks.
    denomTensor = torch.Tensor(sumNumeTensor:size())
  for i = 1,K do
  --first for loop loops through the hidden loop
      contractDenomTensor = sumDenomConv:forward(sumNumeTensor[{{i},{},{}}]) -- is a 1x(Nh/Nw)x(Nh/Nw) tensor
        --Below expands out the tensor to the correct size
      denomTensor[{{i},{},{}}] = sumDenomConv:backward(sumNumeTensor[{{i},{},{}}],contractDenomTensor)
  end
  probTensor = numeTensor:cdiv(denomTensor[{{},{1,Nh},{1,Nh}}])
  unifProbs = torch.rand(K,Nh,Nh)
  --hiddenNodeTensor = torch.le(unifProbs,probTensor):double() --This bit activates the relevant units.
  return torch.le(unifProbs,probTensor):double(), probTensor
end
